(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dashboard-dashboard-module"],{

/***/ "./src/app/layout/dashboard/dashboard-routing.module.ts":
/*!**************************************************************!*\
  !*** ./src/app/layout/dashboard/dashboard-routing.module.ts ***!
  \**************************************************************/
/*! exports provided: DashboardRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardRoutingModule", function() { return DashboardRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _dashboard_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./dashboard.component */ "./src/app/layout/dashboard/dashboard.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _dashboard_component__WEBPACK_IMPORTED_MODULE_2__["DashboardComponent"]
    }
];
var DashboardRoutingModule = /** @class */ (function () {
    function DashboardRoutingModule() {
    }
    DashboardRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], DashboardRoutingModule);
    return DashboardRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/dashboard/dashboard.component.html":
/*!***********************************************************!*\
  !*** ./src/app/layout/dashboard/dashboard.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-card class=\"p-0\">\r\n  <div fxLayout=\"row wrap\">\r\n    <div fxFlex=\"50\" fxFlex.gt-sm=\"20\" fxFlex.sm=\"50\">\r\n      <div class=\"text-center pt-1 pb-1 border-right-light\">\r\n        <mat-icon class=\"text-blue\">people</mat-icon>\r\n        <h4 class=\"m-0 \">20</h4>\r\n        <small class=\"m-0 \"> Total Customers </small>\r\n      </div>\r\n    </div>\r\n    <div fxFlex=\"50\" fxFlex.gt-sm=\"50\" fxFlex.sm=\"50\">\r\n      <div class=\"text-center pt-1 pb-1 border-right-light\">\r\n        <mat-icon class=\"text-blue\">shopping_basket</mat-icon>\r\n        <h4 class=\"m-0 \">45</h4>\r\n        <small class=\"m-0 \"> Total Products </small>\r\n      </div>\r\n    </div>\r\n    <div fxFlex=\"50\" fxFlex.gt-sm=\"30\" fxFlex.sm=\"50\">\r\n      <div class=\"text-center pt-1 pb-1 border-right-light\">\r\n        <mat-icon class=\"text-green\">people</mat-icon>\r\n        <h4 class=\"m-0 \">20</h4>\r\n        <small class=\"m-0 \"> Total Vendors</small>\r\n      </div>\r\n    </div>\r\n    <!-- <div fxFlex=\"50\" fxFlex.gt-sm=\"20\" fxFlex.sm=\"50\">\r\n      <div class=\"text-center pt-1 pb-1 border-right-light\">\r\n        <mat-icon class=\"text-blue\">people</mat-icon>\r\n        <h4 class=\"m-0 \">3010</h4>\r\n        <small class=\"m-0 \">Parents </small>\r\n      </div>\r\n    </div> \r\n    <div fxFlex=\"50\" fxFlex.gt-sm=\"20\" fxFlex.sm=\"50\">\r\n      <div class=\"text-center pt-1 pb-1\">\r\n        <mat-icon class=\"text-red\">favorite</mat-icon>\r\n        <h4 class=\"m-0 \">4070</h4>\r\n        <small class=\"m-0 \">Total Members</small>\r\n      </div>\r\n    </div> -->\r\n  </div>\r\n</mat-card>\r\n\r\n\r\n<!-- <div class=\"mb-20\" fxLayout=\"row\" fxLayout.lt-md=\"column\" fxFlex fxLayoutGap=\"20px\" style=\"cursor: pointer;\">\r\n      <div fxFlex [routerLink]=\"['/customers']\">\r\n          <app-stat [bgClass]=\"'bg-white'\" [icon]=\"'people'\"  [count]=\"customersData.length\" [label]=\"'Total Customers!'\"></app-stat>\r\n      </div>\r\n\r\n\r\n      <div fxFlex [routerLink]=\"['/products']\" >\r\n          <app-stat [bgClass]=\"'success'\" [icon]=\"'shopping_basket'\" [count]=\"productsData.length\" [label]=\"'Total Products!'\"></app-stat>\r\n      </div> \r\n       \r\n       <div fxFlex [routerLink]=\"['/vendors']\">\r\n          <app-stat [bgClass]=\"'warn'\" [icon]=\"'people'\"  [count]=\"vendorsData.length\" [label]=\"'Total Vendors!'\"></app-stat>\r\n      </div>\r\n     \r\n  </div>\r\n   -->\r\n\r\n\r\n\r\n<div class=\"heading\">\r\n  <h2>\r\n    Bar Chart of Customers & Vendors\r\n    <br /><br />\r\n\r\n    <!-- <button (click)=\"getGraphData()\" class=\"btn\">Change Data</button> -->\r\n\r\n  </h2><br />\r\n</div>\r\n<div class=\"graph\" *ngIf=\"barChartData\">\r\n\r\n  <canvas baseChart [datasets]=\"barChartData\" [labels]=\"barChartLabels\" [options]=\"barChartOptions\"\r\n    [colors]=\"barChartColors\" [legend]=\"barChartLegend\" [chartType]=\"barChartType\" (chartHover)=\"chartHovered($event)\"\r\n    (chartClick)=\"chartClicked($event)\"></canvas>\r\n</div>"

/***/ }),

/***/ "./src/app/layout/dashboard/dashboard.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/layout/dashboard/dashboard.component.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "p {\n  font-family: Lato; }\n\n.heading {\n  text-align: center;\n  margin-top: 50px; }\n\n.btn {\n  background-color: #3f51b5;\n  color: white; }\n\n.graph {\n  display: block;\n  margin-bottom: 50px; }\n\n.div.mat-card-header-text.mat-card-subtitle {\n  color: black; }\n\n.customerss {\n  background-color: red;\n  padding: 0px;\n  color: black; }\n\n.total-customers {\n  position: absolute; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L2Rhc2hib2FyZC9DOlxcVXNlcnNcXFZMT0dJQ1xcRGVza3RvcFxcYW5ndWxhclxcbGFhdmFudGFkbWluX2FuZ3VsYXIvc3JjXFxhcHBcXGxheW91dFxcZGFzaGJvYXJkXFxkYXNoYm9hcmQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBMENBO0VBQ0ksa0JBQWlCLEVBQ2xCOztBQUVEO0VBQ0UsbUJBQWlCO0VBQ2pCLGlCQUFnQixFQUNqQjs7QUFFRDtFQUNJLDBCQUF5QjtFQUN6QixhQUFZLEVBQ2Y7O0FBQ0Q7RUFDRyxlQUFjO0VBQ2Isb0JBQW1CLEVBQ25COztBQUNEO0VBQ0MsYUFBWSxFQUNmOztBQUNEO0VBQ0Usc0JBQXFCO0VBQ3JCLGFBQVk7RUFDWixhQUFZLEVBQ2I7O0FBQ0Q7RUFDRSxtQkFBa0IsRUFDbkIiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvZGFzaGJvYXJkL2Rhc2hib2FyZC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIHRhYmxlIHtcclxuLy8gICAgIHdpZHRoOiAxMDAlO1xyXG4vLyB9XHJcbi8vIHRhYmxlLmNhcmQge1xyXG4vLyAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4vLyAgICAgaW1nIHtcclxuLy8gICAgICAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbi8vICAgICAgICAgbWFyZ2luLXRvcDogLTI1cHg7XHJcbi8vICAgICB9XHJcbi8vIH1cclxuLy8gdGFibGUudGFibGUge1xyXG4vLyAgICAgYm94LXNoYWRvdzogMCAzcHggMXB4IC0ycHggcmdiYSgwLCAwLCAwLCAwLjIpLCAwIDJweCAycHggMCByZ2JhKDAsIDAsIDAsIDAuMTQpLFxyXG4vLyAgICAgICAgIDAgMXB4IDVweCAwIHJnYmEoMCwgMCwgMCwgMC4xMik7XHJcbi8vIH1cclxuXHJcbi8vIHRhYmxlLm0yMCB7XHJcbi8vICAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG4vLyAgICAgY29sb3I6IGJsYWNrO1xyXG4vLyB9XHJcblxyXG5cclxuLy8gdGFibGUudGgsdGR7XHJcbi8vICAgICBib3JkZXI6IDFweCBzb2xpZCByZ2IoMTk4LCAyMzEsIDIwMSk7XHJcbi8vICAgcGFkZGluZzogMTBweDtcclxuLy8gfVxyXG4vLyB0cjpudGgtY2hpbGQoZXZlbil7YmFja2dyb3VuZC1jb2xvcjogI2YyZjJmMjt9XHJcblxyXG4vLyB0cjpob3ZlciB7YmFja2dyb3VuZC1jb2xvcjogI2RkZDt9XHJcbi8vIHRoIHtcclxuLy8gICAgIHBhZGRpbmctdG9wOiAyMHB4O1xyXG4vLyAgICAgcGFkZGluZy1ib3R0b206IDEycHg7XHJcbi8vICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4vLyAgICAgYmFja2dyb3VuZC1jb2xvcjogIzRDQUY1MDtcclxuLy8gICAgIGNvbG9yOiB3aGl0ZTtcclxuLy8gICAgIHRleHQtc2l6ZS1hZGp1c3Q6IDIwcHg7XHJcbi8vICAgfVxyXG4vLyAgIHRhYmxlIHtcclxuLy8gICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xyXG4vLyB9XHJcbi8vIGN1cnNvcnsgXHJcbi8vICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbi8vIH1cclxucCB7XHJcbiAgICBmb250LWZhbWlseTogTGF0bztcclxuICB9XHJcblxyXG4gIC5oZWFkaW5ne1xyXG4gICAgdGV4dC1hbGlnbjpjZW50ZXI7XHJcbiAgICBtYXJnaW4tdG9wOiA1MHB4O1xyXG4gIH1cclxuXHJcbiAgLmJ0bntcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzNmNTFiNTtcclxuICAgICAgY29sb3I6IHdoaXRlO1xyXG4gIH1cclxuICAuZ3JhcGh7XHJcbiAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDUwcHg7XHJcbiAgICAgfVxyXG4gICAgIC5kaXYubWF0LWNhcmQtaGVhZGVyLXRleHQubWF0LWNhcmQtc3VidGl0bGUge1xyXG4gICAgICBjb2xvcjogYmxhY2s7XHJcbiAgfVxyXG4gIC5jdXN0b21lcnNze1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmVkO1xyXG4gICAgcGFkZGluZzogMHB4O1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gIH1cclxuICAudG90YWwtY3VzdG9tZXJze1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIH1cclxuIl19 */"

/***/ }),

/***/ "./src/app/layout/dashboard/dashboard.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/layout/dashboard/dashboard.component.ts ***!
  \*********************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_assets_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/assets/api.service */ "./src/assets/api.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(matDialog, http, router, apiservice) {
        this.matDialog = matDialog;
        this.http = http;
        this.router = router;
        this.apiservice = apiservice;
        this.pageNo = 1;
        this.vendorsData = [];
        this.customersData = [];
        this.barChartLabels = [];
        this.barChartData = [];
        this.productsData = [];
        this.isLoading = false;
        this.barChartOptions = {
            scaleShowVerticalLines: false,
            responsive: true
        };
        // public barChartData = [
        //   {data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A'},
        //   {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'}
        // ];
        //  public barChartLabels:string[] = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
        this.barChartType = 'bar';
        this.barChartLegend = true;
        // public barChartPlugins = [pluginDataLabels];
        this.barChartColors = [
            {
                backgroundColor: '#3f51b5',
            },
            {
                backgroundColor: '#4CAF50',
            },
        ];
    }
    // events
    DashboardComponent.prototype.chartClicked = function (e) {
        // console.log(e);
    };
    DashboardComponent.prototype.chartHovered = function (e) {
        // console.log(e);
    };
    // public barChartData = [];
    // openModal1(data) {
    //     const dialogConfig = new MatDialogConfig();
    //     dialogConfig.disableClose = false;
    //     dialogConfig.id = 'modal-component';
    //     dialogConfig.height = '650px';
    //     dialogConfig.width = '600px';
    //     dialogConfig.data = data;
    //     // https://matersial.angular.io/components/dialog/overview
    //     const modalDialog = this.matDialog.open(Modal1Component, dialogConfig);
    //   }
    DashboardComponent.prototype.getGraphData = function () {
        var _this = this;
        this.isLoading = true;
        this.apiservice.graphData().subscribe(function (res) {
            _this.data = res;
            _this.isLoading = false;
            _this.data.data.barChartData.forEach(function (element) {
                // let item ={
                //   data:[],
                //   label:''
                // }
                _this.barChartData.push(element);
                _this.barChartLabels = _this.data.data.labels;
            });
            console.log('graphdata', _this.barChartData);
            //       let barChartData = [
            //         {data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A'},
            //         {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'}
            //       ];
            //       let barChartLabels= ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
            // this.barChartData=barChartData;
            // this.barChartLabels= barChartLabels;
        });
    };
    DashboardComponent.prototype.getUsers = function () {
        var _this = this;
        this.apiservice.getCustomers().subscribe(function (res) {
            _this.customersData = res;
        });
    };
    DashboardComponent.prototype.getVendors = function () {
        var _this = this;
        this.apiservice.getTotalVendors().subscribe(function (res) {
            _this.vendorsData = res;
        });
    };
    DashboardComponent.prototype.getCustomers = function () {
        var _this = this;
        this.apiservice.getTotalCustomers().subscribe(function (res) {
            _this.customersData = res;
        });
    };
    DashboardComponent.prototype.getProducts = function () {
        var _this = this;
        this.apiservice.getTotalProducts().subscribe(function (res) {
            _this.productsData = res;
        });
    };
    DashboardComponent.prototype.ngOnInit = function () {
        // this.getUsers();
        this.getVendors();
        this.getCustomers();
        this.getProducts();
        this.getGraphData();
    };
    DashboardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__(/*! ./dashboard.component.html */ "./src/app/layout/dashboard/dashboard.component.html"),
            styles: [__webpack_require__(/*! ./dashboard.component.scss */ "./src/app/layout/dashboard/dashboard.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            src_assets_api_service__WEBPACK_IMPORTED_MODULE_4__["ApiService"]])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/layout/dashboard/dashboard.module.ts":
/*!******************************************************!*\
  !*** ./src/app/layout/dashboard/dashboard.module.ts ***!
  \******************************************************/
/*! exports provided: DashboardModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardModule", function() { return DashboardModule; });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/grid-list */ "./node_modules/@angular/material/esm5/grid-list.es5.js");
/* harmony import */ var _shared_modules_stat_stat_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../shared/modules/stat/stat.module */ "./src/app/shared/modules/stat/stat.module.ts");
/* harmony import */ var _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./dashboard-routing.module */ "./src/app/layout/dashboard/dashboard-routing.module.ts");
/* harmony import */ var _dashboard_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./dashboard.component */ "./src/app/layout/dashboard/dashboard.component.ts");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-charts */ "./node_modules/ng2-charts/index.js");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(ng2_charts__WEBPACK_IMPORTED_MODULE_8__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var DashboardModule = /** @class */ (function () {
    function DashboardModule() {
    }
    DashboardModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
                _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_6__["DashboardRoutingModule"],
                _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_4__["MatGridListModule"],
                _shared_modules_stat_stat_module__WEBPACK_IMPORTED_MODULE_5__["StatModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"],
                ng2_charts__WEBPACK_IMPORTED_MODULE_8__["ChartsModule"],
                _angular_flex_layout__WEBPACK_IMPORTED_MODULE_2__["FlexLayoutModule"].withConfig({ addFlexToParent: false })
            ],
            declarations: [_dashboard_component__WEBPACK_IMPORTED_MODULE_7__["DashboardComponent"]]
        })
    ], DashboardModule);
    return DashboardModule;
}());



/***/ })

}]);
//# sourceMappingURL=dashboard-dashboard-module.js.map