(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-in": "./node_modules/moment/locale/en-in.js",
	"./en-in.js": "./node_modules/moment/locale/en-in.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./en-sg": "./node_modules/moment/locale/en-sg.js",
	"./en-sg.js": "./node_modules/moment/locale/en-sg.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fil": "./node_modules/moment/locale/fil.js",
	"./fil.js": "./node_modules/moment/locale/fil.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-deva": "./node_modules/moment/locale/gom-deva.js",
	"./gom-deva.js": "./node_modules/moment/locale/gom-deva.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./oc-lnc": "./node_modules/moment/locale/oc-lnc.js",
	"./oc-lnc.js": "./node_modules/moment/locale/oc-lnc.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tk": "./node_modules/moment/locale/tk.js",
	"./tk.js": "./node_modules/moment/locale/tk.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-mo": "./node_modules/moment/locale/zh-mo.js",
	"./zh-mo.js": "./node_modules/moment/locale/zh-mo.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./dashboard/dashboard.module": [
		"./src/app/layout/dashboard/dashboard.module.ts",
		"default~dashboard-dashboard-module~userupdate-userupdate-module",
		"dashboard-dashboard-module"
	],
	"./layout/layout.module": [
		"./src/app/layout/layout.module.ts",
		"default~layout-layout-module~userupdate-userupdate-module",
		"layout-layout-module"
	],
	"./login/login.module": [
		"./src/app/login/login.module.ts"
	],
	"./userupdate/userupdate.module": [
		"./src/app/layout/userupdate/userupdate.module.ts",
		"default~dashboard-dashboard-module~userupdate-userupdate-module",
		"default~layout-layout-module~userupdate-userupdate-module",
		"userupdate-userupdate-module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		var id = ids[0];
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_guard_auth_guard__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./shared/guard/auth.guard */ "./src/app/shared/guard/auth.guard.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        loadChildren: './login/login.module#LoginModule'
    },
    {
        path: '',
        loadChildren: './layout/layout.module#LayoutModule',
    },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
            providers: [_shared_guard_auth_guard__WEBPACK_IMPORTED_MODULE_2__["AuthGuard"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "  \r\n\r\n  <router-outlet></router-outlet>\r\n  "

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = /** @class */ (function () {
    function AppComponent(translate) {
        this.translate = translate;
        translate.setDefaultLang('en');
    }
    AppComponent.prototype.ngOnInit = function () {
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        __metadata("design:paramtypes", [_ngx_translate_core__WEBPACK_IMPORTED_MODULE_1__["TranslateService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: createTranslateLoader, AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createTranslateLoader", function() { return createTranslateLoader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/cdk/layout */ "./node_modules/@angular/cdk/esm5/layout.es5.js");
/* harmony import */ var _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/cdk/overlay */ "./node_modules/@angular/cdk/esm5/overlay.es5.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ngx-translate/http-loader */ "./node_modules/@ngx-translate/http-loader/fesm5/ngx-translate-http-loader.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _validator_directive__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./validator.directive */ "./src/app/validator.directive.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _login_login_module__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./login/login.module */ "./src/app/login/login.module.ts");
/* harmony import */ var _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/progress-spinner */ "./node_modules/@angular/material/esm5/progress-spinner.es5.js");
/* harmony import */ var ngx_pagination__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ngx-pagination */ "./node_modules/ngx-pagination/dist/ngx-pagination.js");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ng2-charts */ "./node_modules/ng2-charts/index.js");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(ng2_charts__WEBPACK_IMPORTED_MODULE_16__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

















var createTranslateLoader = function (http) {
    /* for development
    return new TranslateHttpLoader(
        http,
        '/start-javascript/sb-admin-material/master/dist/assets/i18n/',
        '.json'
    );*/
    return new _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_8__["TranslateHttpLoader"](http, './assets/i18n/', '.json');
};
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [_app_component__WEBPACK_IMPORTED_MODULE_10__["AppComponent"], _validator_directive__WEBPACK_IMPORTED_MODULE_11__["ValidatorDirective"]],
            imports: [
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginatorModule"],
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["BrowserModule"],
                _login_login_module__WEBPACK_IMPORTED_MODULE_13__["LoginModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_12__["ReactiveFormsModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_9__["AppRoutingModule"],
                _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_0__["LayoutModule"],
                _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_1__["OverlayModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"],
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["BrowserModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatOptionModule"],
                _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_14__["MatProgressSpinnerModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__["BrowserAnimationsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_12__["FormsModule"],
                ngx_pagination__WEBPACK_IMPORTED_MODULE_15__["NgxPaginationModule"],
                ng2_charts__WEBPACK_IMPORTED_MODULE_16__["ChartsModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"].forRoot({
                    loader: {
                        provide: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateLoader"],
                        useFactory: createTranslateLoader,
                        deps: [_angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"]]
                    }
                })
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_10__["AppComponent"]],
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/login/login-routing.module.ts":
/*!***********************************************!*\
  !*** ./src/app/login/login-routing.module.ts ***!
  \***********************************************/
/*! exports provided: LoginRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginRoutingModule", function() { return LoginRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _login_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./login.component */ "./src/app/login/login.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _login_component__WEBPACK_IMPORTED_MODULE_2__["LoginComponent"]
    }
];
var LoginRoutingModule = /** @class */ (function () {
    function LoginRoutingModule() {
    }
    LoginRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], LoginRoutingModule);
    return LoginRoutingModule;
}());



/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "   \r\n   <div class=\"login-page\">\r\n       \r\n    <div class=\"content\">\r\n        <!-- <h1 class=\"app-name\">Laavant Admin</h1> -->\r\n        \r\n      \r\n\r\n        <form class=\"login-form\" fxFlex [formGroup]=\"loginForm\" (ngSubmit)=\"onSubmit()\">\r\n            <div class=\"text-center\">\r\n                <h2 class=\"app-name\">Laavant Admin</h2>\r\n            </div>\r\n            <div fxFlex  fxlayout=\"row\" fxlayout.lt-md=\"column\">\r\n                <div fxFlexFill>\r\n                    <mat-form-field class=\"w-100\">\r\n                        <input matInput placeholder=\"Email\" type=\"email\" formControlName=\"email1\" [ngClass]=\"{'is-invalid': submitted && f.email1.errors}\" [(ngModel)]=\"credentials.email\">\r\n                    \r\n                    </mat-form-field>\r\n                    <div *ngIf=\"submitted && f.email1.errors\">\r\n                        <div *ngIf=\"f.email1.errors.required\"></div></div>\r\n                </div>\r\n            </div>\r\n            <div fxFlex  fxLayout=\"row\" fxLayout.lt-md=\"column\">\r\n                <div fxFlexFill>\r\n                    <mat-form-field class=\"w-100\">\r\n                        <input matInput type=\"password\" placeholder=\"Password\" formControlName=\"password1\" [ngClass]=\"{'is-invalid': submitted && f.password1.errors}\" [(ngModel)]=\"credentials.password\">\r\n                    </mat-form-field>\r\n                    <div *ngIf=\"submitted && f.password1.errors\">\r\n                        <div *ngIf=\"f.password1.errors.required\"></div></div>\r\n                </div>\r\n            </div>\r\n            <!-- <div fxFlex  fxLayout=\"row\" fxLayout.lt-md=\"column\" style=\"margin: 20px 0 30px 0\">\r\n                <div fxFlex>\r\n                    <mat-checkbox color=\"primary\">Remember Me</mat-checkbox>\r\n                </div>\r\n                <div fxFlex  fxLayoutAlign=\"end\">\r\n                    <a href=\"javascript:void(0)\">Forget Password</a>\r\n                </div>\r\n            </div> -->\r\n           \r\n            <div *ngIf=\"isLoading\" >\r\n                <div class=\"loader\"></div>\r\n            </div>\r\n         \r\n            <div fxFlex  fxLayout=\"row\" fxLayout.lt-md=\"column\">\r\n                <div fxFlexFill>\r\n                    <button mat-raised-button color=\"primary\"  [disabled]=\"loginForm.invalid\" class=\"w-100\">Login</button>\r\n                </div>\r\n            </div>\r\n\r\n        </form>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/login/login.component.scss":
/*!********************************************!*\
  !*** ./src/app/login/login.component.scss ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".login-page {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  height: 100%;\n  position: relative; }\n  .login-page .content {\n    z-index: 1;\n    display: flex;\n    align-items: center;\n    justify-content: center; }\n  .login-page .content .app-name {\n      margin-top: 0px;\n      padding-bottom: 10px;\n      font-size: 32px;\n      color: #3f51b5; }\n  .login-page .content .login-form {\n      padding: 40px;\n      background: #fff;\n      /*width: 500px;*/\n      box-shadow: 0 0 10px #ddd; }\n  .login-page .content .login-form input:-webkit-autofill {\n        box-shadow: 0 0 0 30px white inset; }\n  .login-page:after {\n    content: '';\n    background: #3f51b5;\n    position: absolute;\n    top: 0;\n    left: 0;\n    bottom: 50%;\n    right: 0; }\n  .login-page:before {\n    content: '';\n    background: #3f51b5;\n    position: absolute;\n    top: 50%;\n    left: 0;\n    bottom: 0;\n    right: 0; }\n  .text-center {\n  text-align: center; }\n  .w-100 {\n  width: 100%; }\n  @media screen and (max-width: 667px) {\n  .login-page[_ngcontent-c14] .content[_ngcontent-c14] .login-form[_ngcontent-c14] {\n    width: 100% !important; } }\n  .loader {\n  position: fixed;\n  left: 48%;\n  top: 50%;\n  border: 5px solid #f3f3f3;\n  border-radius: 50%;\n  border-top: 5px solid #3f51b5;\n  width: 30px;\n  height: 30px;\n  -webkit-animation: spin s linear infinite;\n  /* Safari */\n  animation: spin 1s linear infinite; }\n  /* Safari */\n  @-webkit-keyframes spin {\n  0% {\n    -webkit-transform: rotate(0deg); }\n  100% {\n    -webkit-transform: rotate(360deg); } }\n  @keyframes spin {\n  0% {\n    transform: rotate(0deg); }\n  100% {\n    transform: rotate(360deg); } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naW4vQzpcXFVzZXJzXFxWTE9HSUNcXERlc2t0b3BcXGFuZ3VsYXJcXGxhYXZhbnRhZG1pbl9hbmd1bGFyL3NyY1xcYXBwXFxsb2dpblxcbG9naW4uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxjQUFhO0VBQ2Isb0JBQW1CO0VBQ25CLHdCQUF1QjtFQUN2QixhQUFZO0VBQ1osbUJBQWtCLEVBd0NyQjtFQTdDRDtJQU9RLFdBQVU7SUFDVixjQUFhO0lBQ2Isb0JBQW1CO0lBQ25CLHdCQUF1QixFQWUxQjtFQXpCTDtNQVlZLGdCQUFlO01BQ2YscUJBQW9CO01BQ3BCLGdCQUFlO01BQ2YsZUFBYyxFQUNqQjtFQWhCVDtNQW1CWSxjQUFhO01BQ2IsaUJBQWdCO01BQ2hCLGlCQUFpQjtNQUNqQiwwQkFBeUIsRUFFNUI7RUF4QlQ7UUF1QnFDLG1DQUFrQyxFQUFHO0VBdkIxRTtJQTRCUSxZQUFXO0lBQ1gsb0JBQWtCO0lBQ2xCLG1CQUFrQjtJQUNsQixPQUFNO0lBQ04sUUFBTztJQUNQLFlBQVc7SUFDWCxTQUFRLEVBQ1g7RUFuQ0w7SUFxQ1EsWUFBVztJQUNYLG9CQUFtQjtJQUNuQixtQkFBa0I7SUFDbEIsU0FBUTtJQUNSLFFBQU87SUFDUCxVQUFTO0lBQ1QsU0FBUSxFQUNYO0VBRUw7RUFDSSxtQkFBa0IsRUFDckI7RUFDRDtFQUNJLFlBQVcsRUFDZDtFQUdDO0VBQ0E7SUFDRSx1QkFBc0IsRUFFekIsRUFBQTtFQUVDO0VBQ0UsZ0JBQWU7RUFDZixVQUFRO0VBQ1IsU0FBUTtFQUNSLDBCQUF5QjtFQUN6QixtQkFBa0I7RUFDbEIsOEJBQTZCO0VBQzdCLFlBQVc7RUFDWCxhQUFZO0VBQ1osMENBQXlDO0VBQUUsWUFBWTtFQUN2RCxtQ0FBa0MsRUFFbkM7RUFFRCxZQUFZO0VBQ1o7RUFDRTtJQUFLLGdDQUErQixFQUFBO0VBQ3BDO0lBQU8sa0NBQWlDLEVBQUEsRUFBQTtFQUcxQztFQUNFO0lBQUssd0JBQXVCLEVBQUE7RUFDNUI7SUFBTywwQkFBeUIsRUFBQSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvbG9naW4vbG9naW4uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubG9naW4tcGFnZSB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgLmNvbnRlbnQge1xyXG4gICAgICAgIHotaW5kZXg6IDE7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIC5hcHAtbmFtZSB7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDBweDtcclxuICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDEwcHg7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMzJweDtcclxuICAgICAgICAgICAgY29sb3I6ICMzZjUxYjU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgXHJcbiAgICAgICAgLmxvZ2luLWZvcm0ge1xyXG4gICAgICAgICAgICBwYWRkaW5nOiA0MHB4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZmZmO1xyXG4gICAgICAgICAgICAvKndpZHRoOiA1MDBweDsqL1xyXG4gICAgICAgICAgICBib3gtc2hhZG93OiAwIDAgMTBweCAjZGRkO1xyXG4gICAgICAgICAgICBpbnB1dDotd2Via2l0LWF1dG9maWxsIHsgYm94LXNoYWRvdzogMCAwIDAgMzBweCB3aGl0ZSBpbnNldDt9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgICY6YWZ0ZXIge1xyXG4gICAgICAgIGNvbnRlbnQ6ICcnO1xyXG4gICAgICAgIGJhY2tncm91bmQ6IzNmNTFiNTtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgdG9wOiAwO1xyXG4gICAgICAgIGxlZnQ6IDA7XHJcbiAgICAgICAgYm90dG9tOiA1MCU7XHJcbiAgICAgICAgcmlnaHQ6IDA7XHJcbiAgICB9XHJcbiAgICAmOmJlZm9yZSB7XHJcbiAgICAgICAgY29udGVudDogJyc7XHJcbiAgICAgICAgYmFja2dyb3VuZDogIzNmNTFiNTtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgdG9wOiA1MCU7XHJcbiAgICAgICAgbGVmdDogMDtcclxuICAgICAgICBib3R0b206IDA7XHJcbiAgICAgICAgcmlnaHQ6IDA7XHJcbiAgICB9XHJcbn1cclxuLnRleHQtY2VudGVyIHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4udy0xMDAge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcblxyXG4gIEBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDY2N3B4KSB7XHJcbiAgLmxvZ2luLXBhZ2VbX25nY29udGVudC1jMTRdIC5jb250ZW50W19uZ2NvbnRlbnQtYzE0XSAubG9naW4tZm9ybVtfbmdjb250ZW50LWMxNF0ge1xyXG4gICAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcclxuICAgIFxyXG59XHJcbiAgfVxyXG4gIC5sb2FkZXIge1xyXG4gICAgcG9zaXRpb246IGZpeGVkO1xyXG4gICAgbGVmdDo0OCU7XHJcbiAgICB0b3A6IDUwJTtcclxuICAgIGJvcmRlcjogNXB4IHNvbGlkICNmM2YzZjM7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICBib3JkZXItdG9wOiA1cHggc29saWQgIzNmNTFiNTtcclxuICAgIHdpZHRoOiAzMHB4O1xyXG4gICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgLXdlYmtpdC1hbmltYXRpb246IHNwaW4gcyBsaW5lYXIgaW5maW5pdGU7IC8qIFNhZmFyaSAqL1xyXG4gICAgYW5pbWF0aW9uOiBzcGluIDFzIGxpbmVhciBpbmZpbml0ZTtcclxuXHJcbiAgfVxyXG4gIFxyXG4gIC8qIFNhZmFyaSAqL1xyXG4gIEAtd2Via2l0LWtleWZyYW1lcyBzcGluIHtcclxuICAgIDAlIHsgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTsgfVxyXG4gICAgMTAwJSB7IC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMzYwZGVnKTsgfVxyXG4gIH1cclxuICBcclxuICBAa2V5ZnJhbWVzIHNwaW4ge1xyXG4gICAgMCUgeyB0cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTsgfVxyXG4gICAgMTAwJSB7IHRyYW5zZm9ybTogcm90YXRlKDM2MGRlZyk7IH1cclxuICB9Il19 */"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var src_assets_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/assets/api.service */ "./src/assets/api.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LoginComponent = /** @class */ (function () {
    function LoginComponent(router, fb, http, apiservice) {
        this.router = router;
        this.fb = fb;
        this.http = http;
        this.apiservice = apiservice;
        this.userData = [];
        this.isLoading = false;
        this.credentials = {
            email: '',
            password: ''
        };
        this.submitted = false;
    }
    LoginComponent.prototype.login = function () {
        var _this = this;
        if (this.submitted) {
            this.isLoading = true;
            this.apiservice.login(this.credentials).subscribe(function (res) {
                if (res.isSuccess === false) {
                    alert('Wrong username or password!');
                    return _this.isLoading = false;
                }
                else {
                    _this.userData = res;
                    _this.isLoading = false;
                    localStorage.setItem('token', _this.userData.token);
                    if (_this.userData.role === 'admin') {
                        _this.router.navigate(['/dashboard']);
                    }
                    else {
                        alert('you are not admin');
                    }
                }
            });
        }
    };
    LoginComponent.prototype.ngOnInit = function () {
        this.loginForm = this.fb.group({
            email1: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            password1: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
    };
    Object.defineProperty(LoginComponent.prototype, "f", {
        get: function () {
            return this.loginForm.controls;
        },
        enumerable: true,
        configurable: true
    });
    LoginComponent.prototype.onSubmit = function () {
        this.submitted = true;
        return this.login();
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/login/login.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"],
            src_assets_api_service__WEBPACK_IMPORTED_MODULE_4__["ApiService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/login/login.module.ts":
/*!***************************************!*\
  !*** ./src/app/login/login.module.ts ***!
  \***************************************/
/*! exports provided: LoginModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginModule", function() { return LoginModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _login_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./login-routing.module */ "./src/app/login/login-routing.module.ts");
/* harmony import */ var _login_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/progress-spinner */ "./node_modules/@angular/material/esm5/progress-spinner.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var LoginModule = /** @class */ (function () {
    function LoginModule() {
    }
    LoginModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _login_routing_module__WEBPACK_IMPORTED_MODULE_4__["LoginRoutingModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatButtonModule"],
                _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_7__["MatProgressSpinnerModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"],
                _angular_flex_layout__WEBPACK_IMPORTED_MODULE_2__["FlexLayoutModule"].withConfig({ addFlexToParent: false })
            ],
            declarations: [_login_component__WEBPACK_IMPORTED_MODULE_5__["LoginComponent"]]
        })
    ], LoginModule);
    return LoginModule;
}());



/***/ }),

/***/ "./src/app/shared/guard/auth.guard.ts":
/*!********************************************!*\
  !*** ./src/app/shared/guard/auth.guard.ts ***!
  \********************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AuthGuard = /** @class */ (function () {
    function AuthGuard(router) {
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function () {
        if (localStorage.getItem('token')) {
            return true;
        }
        this.router.navigate(['/login']);
        return false;
    };
    AuthGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/validator.directive.ts":
/*!****************************************!*\
  !*** ./src/app/validator.directive.ts ***!
  \****************************************/
/*! exports provided: ValidatorDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ValidatorDirective", function() { return ValidatorDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ValidatorDirective = /** @class */ (function () {
    function ValidatorDirective() {
    }
    ValidatorDirective_1 = ValidatorDirective;
    ValidatorDirective.prototype.validate = function (control) {
        var controlToCompare = control.parent.get(this.appValidator);
        if (controlToCompare && controlToCompare.value !== control.value) {
            return { 'notEqual': true };
        }
        return null;
    };
    var ValidatorDirective_1;
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], ValidatorDirective.prototype, "appValidator", void 0);
    ValidatorDirective = ValidatorDirective_1 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[appValidator]',
            providers: [{
                    provide: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NG_VALIDATORS"],
                    useExisting: ValidatorDirective_1,
                    multi: true
                }]
        })
    ], ValidatorDirective);
    return ValidatorDirective;
}());



/***/ }),

/***/ "./src/assets/api.service.ts":
/*!***********************************!*\
  !*** ./src/assets/api.service.ts ***!
  \***********************************/
/*! exports provided: ApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiService", function() { return ApiService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/internal/Subject */ "./node_modules/rxjs/internal/Subject.js");
/* harmony import */ var rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ApiService = /** @class */ (function () {
    function ApiService(http) {
        this.http = http;
        this.root = src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].apiUrls.reports;
        this.userResponse = {};
        this.productResponse = {};
        this.categoryResponse = {};
        this.customerResponse = {};
        this.subCategoryResponse = {};
        this.productCategory = {};
        this.vendorResponse = {};
        this.productRes = {};
        this.graphResponse = {};
        this.planResponse = {};
        this.featureResponse = {};
        this.planByIdResponse = {};
        this.userByIdResponse = {};
        this.featuresByIdResponse = {};
    }
    ApiService.prototype.login = function (model) {
        var subject = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.http.post(this.root + "/users/login", model, { headers: null }).subscribe(function (responseData) {
            // if (responseData.statusCode !== 200) {
            //   throw new Error('This request has failed ' + responseData.statusCode);
            // }
            // const dataModel = responseData.json();
            // if (!dataModel.isSuccess) {
            //   if (responseData.statusCode === 200) {
            //     throw new Error(dataModel.code || dataModel.message || 'failed');
            //   } else {
            //     throw new Error(responseData.statusCode + '');
            //   }
            // }
            subject.next(responseData.data);
        }, function (error) {
            // console.log('errrrrrrrrrr', error.error);
            subject.next(error.error);
        });
        return subject.asObservable();
    };
    ApiService.prototype.getUsers = function (pageNo, pageSize) {
        var _this = this;
        var subject = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        // tslint:disable-next-line:max-line-length
        this.http.get(this.root + "/users/list?pageNo= " + pageNo + "&pageSize= " + pageSize + "&role=customer", { headers: null }).subscribe(function (responseData) {
            _this.userResponse = responseData;
            subject.next(_this.userResponse.items);
        });
        return subject.asObservable();
    };
    ApiService.prototype.getCustomers = function () {
        var _this = this;
        var subject = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        // tslint:disable-next-line:max-line-length
        this.http.get(this.root + "/users/list?pageNo=1&pageSize=1000&role=customer", { headers: null }).subscribe(function (responseData) {
            _this.userResponse = responseData;
            subject.next(_this.userResponse.data);
        });
        return subject.asObservable();
    };
    ApiService.prototype.getTotalCustomers = function () {
        var _this = this;
        var subject = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        // tslint:disable-next-line:max-line-length
        this.http.get(this.root + "/users/list?pageNo=1&pageSize=1000&role=customer", { headers: null }).subscribe(function (responseData) {
            _this.userResponse = responseData;
            subject.next(_this.userResponse.items);
        });
        return subject.asObservable();
    };
    ApiService.prototype.updateUser = function (id, user) {
        var _this = this;
        // tslint:disable-next-line:no-debugger
        // debugger;
        var subject = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        var token = localStorage.getItem('token');
        var header = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'x-access-token': token
            })
        };
        this.http.put(this.root + "/users/update/" + id, user, header).subscribe(function (responseData) {
            _this.userResponse = responseData;
            console.log('updateeeeeee', responseData);
            // console.log('api  to response', this.userResponse);
            subject.next(_this.userResponse);
        });
        return subject.asObservable();
    };
    ApiService.prototype.getVendors = function (pageNo, pageSize) {
        var _this = this;
        var subject = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        // tslint:disable-next-line:max-line-length
        this.http.get(this.root + "/users/list?pageNo= " + pageNo + "&pageSize= " + pageSize + "&role=vendor", { headers: null }).subscribe(function (responseData) {
            _this.userResponse = responseData;
            subject.next(_this.userResponse.items);
        });
        return subject.asObservable();
    };
    ApiService.prototype.getTotalVendors = function () {
        var _this = this;
        var subject = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        // tslint:disable-next-line:max-line-length
        this.http.get(this.root + "/users/list?pageNo=1&pageSize=1000&role=vendor", { headers: null }).subscribe(function (responseData) {
            _this.userResponse = responseData;
            subject.next(_this.userResponse.items);
        });
        return subject.asObservable();
    };
    ApiService.prototype.getProducts = function (pageNo, pageSize) {
        var _this = this;
        var subject = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        // inprogress
        this.http.get(this.root + "/products/list?pageNo=" + pageNo + "&pageSize=" + pageSize + "&role=admin").subscribe(function (responseData) {
            _this.productResponse = responseData;
            subject.next(_this.productResponse.items);
        }, function (error) {
            // console.log('errrrrrrrrrr', error.error);
            subject.next(error.error);
        });
        return subject.asObservable();
    };
    ApiService.prototype.getTotalProducts = function () {
        var _this = this;
        var subject = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.http.get(this.root + "/products/list?pageNo=1&pageSize=500").subscribe(function (responseData) {
            _this.productResponse = responseData;
            subject.next(_this.productResponse.items);
        });
        return subject.asObservable();
    };
    // updateProductImage(id,data): Observable<Product[]> {
    //   const subject = new Subject<Product[]>();
    //   const token = localStorage.getItem('token');
    //   const header = {
    //     headers: new HttpHeaders({
    //         'Content-Type': 'application/json',
    //         'x-access-token': token
    //     })
    //   };
    //    this.http.put(`${this.root}/uploads/image/product/${id}`, header ).subscribe((responseData) => {
    //     this.productResponse = responseData;
    //     console.log('image', responseData)
    //     subject.next(this.productResponse.items);
    //   }, (error) => {
    //     subject.next(error.error);
    //        console.log('errrrrrrrrrr', error.error);
    // }  );
    //   return subject.asObservable();
    // }
    // uploadProductImage(id, model): Observable<any> {
    //   const subject = new Subject<any>();
    //   const token = localStorage.getItem('token');
    //   const header = {
    //     headers: new HttpHeaders({
    //       'enctype': 'multipart/form-data',
    //       'Accept': 'application/json',
    //       'x-access-token': this.token
    //     })
    //   };
    //   this.http.put(`${this.root}/uploads/image/product/${id}`, model, header).subscribe((responseData) => {
    //     this.productResponse = responseData;
    //     console.log('imagesssss', this.productResponse);
    //     subject.next(this.productResponse.items);
    //   }, (error) => {
    //     subject.next(error.error);
    //     console.log('errrrrrrrrrr', error.error);
    //   });
    //   return subject.asObservable();
    // }
    //============================== uploadpdf========================================>
    ApiService.prototype.uploadPDF = function (id, model) {
        var _this = this;
        var subject = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        var token = localStorage.getItem('token');
        var header = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'enctype': 'multipart/form-data',
                'Accept': 'application/json',
                'x-access-token': this.token
            })
        };
        this.http.put(this.root + "/products/uploadPdf/" + id, model, header).subscribe(function (responseData) {
            _this.productResponse = responseData;
            console.log('pdffff', _this.productResponse);
            subject.next(_this.productResponse.items);
        }, function (error) {
            subject.next(error.error);
            console.log('errrrrrrrrrr', error.error);
        });
        return subject.asObservable();
    };
    // updateUserImage(data): Observable<Product[]> {
    //   const subject = new Subject<Product[]>();
    //    this.http.put(`${this.root}/users/uploadProfilePic` , data ).subscribe((responseData) => {
    //     this.productResponse = responseData;
    //     subject.next(this.productResponse.items);
    //   }, (error) => {
    //     subject.next(error.error);
    // }  );
    //   return subject.asObservable();
    // }
    ApiService.prototype.getProductCategory = function () {
        var _this = this;
        var subject = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        var token = localStorage.getItem('token');
        var header = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'x-access-token': token
            })
        };
        this.http.get(this.root + "/categories", header).subscribe(function (responseData) {
            _this.categoryResponse = responseData;
            console.log('category', responseData);
            subject.next(_this.categoryResponse);
        });
        return subject.asObservable();
    };
    ApiService.prototype.getProductSubCategory = function (id) {
        var _this = this;
        var subject = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        var token = localStorage.getItem('token');
        var header = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'x-access-token': token
            })
        };
        console.log('id', id);
        this.http.get(this.root + "/subCategories/getByCategory?id=" + id, header).subscribe(function (responseData) {
            _this.subCategoryResponse = responseData;
            console.log('subcategory', responseData);
            subject.next(_this.subCategoryResponse);
        });
        return subject.asObservable();
    };
    ApiService.prototype.getCustomerGroup = function () {
        var _this = this;
        var subject = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        var token = localStorage.getItem('token');
        var header = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'x-access-token': token
            })
        };
        this.http.get(this.root + "/categories", header).subscribe(function (responseData) {
            _this.categoryResponse = responseData;
            // console.log('customer', responseData);
            subject.next(_this.categoryResponse.data);
        });
        return subject.asObservable();
    };
    ApiService.prototype.getVendorProducts = function (vendorId, pageNo, pageSize) {
        var _this = this;
        var subject = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        // inprogress
        // tslint:disable-next-line:max-line-length
        this.http.get(this.root + "/products/listByVendor?pageNo= " + pageNo + "&pageSize= " + pageSize + "&vendorId=" + vendorId).subscribe(function (responseData) {
            _this.productResponse = responseData;
            subject.next(_this.productResponse.items);
        });
        return subject.asObservable();
    };
    ApiService.prototype.addProducts = function (data) {
        var _this = this;
        var subject = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        var token = localStorage.getItem('token');
        var header = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'x-access-token': token
            })
        };
        this.http.post(this.root + "/products/addProduct", data, header).subscribe(function (res) {
            _this.productResponse = res;
            console.log('addproductresponse>>>>>>>>', res);
            subject.next(_this.productResponse);
        }, function (error) {
            //  console.log('errrrrrrrrrr', error.error);
            subject.next(error.error);
        });
        return subject.asObservable();
    };
    ApiService.prototype.addNewCategory1 = function (model) {
        var _this = this;
        var subject = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        var token = localStorage.getItem('token');
        var header = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'x-access-token': token
            })
        };
        this.http.post(this.root + "/categories", model, header).subscribe(function (res) {
            _this.categoryResponse = res;
            console.log('addcategory', res);
            subject.next(_this.categoryResponse);
        }, function (error) {
            console.log('errrrrrrrrrr', error.error);
            subject.next(error.error);
        });
        return subject.asObservable();
    };
    ApiService.prototype.addNewCategory = function (model) {
        var _this = this;
        var subject = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        var token = localStorage.getItem('token');
        var header = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'x-access-token': token
            })
        };
        this.http.post(this.root + "/subCategories", model, header).subscribe(function (res) {
            _this.subCategoryResponse = res;
            console.log('addsubcategory', res);
            subject.next(_this.subCategoryResponse);
        }, function (error) {
            console.log('errrrrrrrrrr', error.error);
            subject.next(error.error);
        });
        return subject.asObservable();
    };
    ApiService.prototype.updateProduct = function (id, model) {
        var _this = this;
        var subject = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        var token = localStorage.getItem('token');
        var header = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'x-access-token': token
            })
        };
        // console.log(this.productResponse);
        this.http.put(this.root + "/products/update/" + id, model).subscribe(function (responseData) {
            _this.productResponse = responseData;
            console.log('********', _this.productResponse);
            subject.next(_this.productResponse);
        }, function (error) {
            console.log('errrrrrrrrrr', error.error);
            subject.next(error.error);
        });
        return subject.asObservable();
    };
    ApiService.prototype.addCustomers = function (data) {
        var _this = this;
        var subject = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        var token = localStorage.getItem('token');
        var header = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'x-access-token': token
            })
        };
        this.http.post(this.root + "/users/register", data).subscribe(function (responseData) {
            _this.customerResponse = responseData;
            // console.log(this.productResponse);
            subject.next(_this.customerResponse);
        }, function (error) {
            console.log('errrrrrrrrrr', error.error);
            subject.next(error.error);
        });
        return subject.asObservable();
    };
    ApiService.prototype.addVendors = function (data) {
        var _this = this;
        var subject = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        var token = localStorage.getItem('token');
        var header = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'x-access-token': token
            })
        };
        this.http.post(this.root + "/users/register", data).subscribe(function (responseData) {
            _this.vendorResponse = responseData;
            console.log(_this.productResponse);
            subject.next(_this.vendorResponse);
        }, function (error) {
            console.log('errrrrrrrrrr', error.error);
            subject.next(error.error);
        });
        return subject.asObservable();
    };
    ApiService.prototype.assignVendor = function (productId, model) {
        var _this = this;
        var subject = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.http.put(this.root + "/products/asignVendor/" + productId, model).subscribe(function (responseData) {
            _this.productResponse = responseData;
            // console.log(this.productResponse);
            subject.next(_this.productResponse);
        }, function (error) {
            console.log('errrrrrrrrrr', error.error);
            subject.next(error.error);
        });
        return subject.asObservable();
    };
    ApiService.prototype.graphData = function () {
        var _this = this;
        var subject = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        var token = localStorage.getItem('token');
        var header = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'x-access-token': token
            })
        };
        this.http.get(this.root + "/counts/monthly/users", header).subscribe(function (responseData) {
            _this.graphResponse = responseData;
            console.log(_this.graphResponse);
            subject.next(_this.graphResponse);
        }, function (error) {
            console.log('errrrrrrrrrr', error.error);
            subject.next(error.error);
        });
        return subject.asObservable();
    };
    ApiService.prototype.addPlan = function (data) {
        var _this = this;
        var subject = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        var token = localStorage.getItem('token');
        var header = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'x-access-token': token
            })
        };
        this.http.post(this.root + "/plans", data, header).subscribe(function (responseData) {
            _this.planResponse = responseData;
            // console.log(this.planResponse);
            console.log('plans@@@', responseData);
            subject.next(_this.planResponse);
        }, function (error) {
            console.log('errrrrrrrrrr', error.error);
            subject.next(error.error);
        });
        return subject.asObservable();
    };
    ApiService.prototype.getPlanList = function () {
        var _this = this;
        var subject = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        var token = localStorage.getItem('token');
        var header = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'x-access-token': token
            })
        };
        this.http.get(this.root + "/plans?role=customer&status=active", header).subscribe(function (responseData) {
            _this.planResponse = responseData;
            // console.log('planslist', responseData);
            console.log('planlist', _this.planResponse);
            subject.next(_this.planResponse.data);
        });
        return subject.asObservable();
    };
    ApiService.prototype.getPlanById = function (id) {
        var _this = this;
        var subject = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        var token = localStorage.getItem('token');
        var header = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'x-access-token': token
            })
        };
        this.http.get(this.root + "/plans/getById/" + id, header).subscribe(function (responseData) {
            _this.planByIdResponse = responseData;
            // console.log('planslist', responseDa ta);
            // console.log('planlist', this.planByIdResponse);
            subject.next(_this.planByIdResponse.data);
        });
        return subject.asObservable();
    };
    ApiService.prototype.getFeaturesList = function () {
        var _this = this;
        var subject = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        var token = localStorage.getItem('token');
        var header = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'x-access-token': token
            })
        };
        this.http.get(this.root + "/features?role=customer&status=active", header).subscribe(function (responseData) {
            _this.featureResponse = responseData;
            // console.log('featurelist', responseData);
            console.log('featurelist', _this.featureResponse);
            subject.next(_this.featureResponse.data);
        });
        return subject.asObservable();
    };
    ApiService.prototype.updatePlan = function (id, data) {
        var _this = this;
        var subject = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        var token = localStorage.getItem('token');
        var header = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'x-access-token': token
            })
        };
        this.http.put(this.root + "/plans/" + id, data, header).subscribe(function (responseData) {
            _this.planResponse = responseData;
            console.log('updated plan', responseData);
            // console.log('api  to response', this.planResponse);
            subject.next(_this.planResponse);
        });
        return subject.asObservable();
    };
    ApiService.prototype.updateFeaturesList = function (id, plan) {
        var _this = this;
        var subject = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        var token = localStorage.getItem('token');
        var header = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'x-access-token': token
            })
        };
        this.http.put(this.root + "/features/" + id, plan, header).subscribe(function (responseData) {
            _this.featureResponse = responseData;
            console.log('updated feature', responseData);
            // console.log('api  to response', this.planResponse);
            subject.next(_this.featureResponse);
        });
        return subject.asObservable();
    };
    ApiService.prototype.getUserById = function (id) {
        var _this = this;
        var subject = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        var token = localStorage.getItem('token');
        var header = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'x-access-token': token
            })
        };
        this.http.get(this.root + "/users/getById/" + id, header).subscribe(function (responseData) {
            _this.userByIdResponse = responseData;
            // console.log('userlist=====', responseData);
            // console.log('userlist', this.userByIdResponse);
            subject.next(_this.userByIdResponse.data);
        });
        return subject.asObservable();
    };
    ApiService.prototype.getFeaturesById = function (id) {
        var _this = this;
        var subject = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        var token = localStorage.getItem('token');
        var header = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'x-access-token': token
            })
        };
        this.http.get(this.root + "/features/getById/" + id, header).subscribe(function (responseData) {
            _this.featuresByIdResponse = responseData;
            // console.log('planslist', responseDa ta);
            console.log('featuresbyId', _this.featuresByIdResponse);
            subject.next(_this.featuresByIdResponse.data);
        });
        return subject.asObservable();
    };
    ApiService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
    ], ApiService);
    return ApiService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    silent: false,
    apiUrls: {
        reports: 'http://93.188.167.68:6100/api',
        master: 'http://93.188.167.68:6100/api',
    },
    name: 'dev'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\VLOGIC\Desktop\angular\laavantadmin_angular\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map