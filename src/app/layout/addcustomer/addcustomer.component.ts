import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/assets/api.service';
import { Router } from '@angular/router';



import { fileURLToPath } from 'url';

@Component({
  selector: 'app-addcustomer',
  templateUrl: './addcustomer.component.html',
  styleUrls: ['./addcustomer.component.scss']
})
export class AddcustomerComponent implements OnInit {
  //   categories: Category[] = [
  //     {id: 1, name: 'shoes'},
  //     {id: 2, name: 'clothes'},
  //     {id: 3, name: 'bags'},
  //  ];
  submitted = false;
  isLoading = false;
  isSHowGroup = true;
  addcustomerForm: FormGroup;
  customerData: any = {
    firstName: '',
    lastName: '',
    email: '',
    mobileNo: '',
    password: '',
    customerGroup: '',
    role: 'customer',
    dob: '',
    anniversary: ''
  };
  productCategory: any = {};
  customerResponse: any = {};
  clickMessage: string;
  isSHowGroup1 = true;
  customerGroup: any;
  id: any;
  responseData: any;
  getaddCustomer: any;
  constructor(private fb: FormBuilder,
    private apiservice: ApiService,
    private router: Router, ) { }

  // customergroup = ['Electrical', 'Concrete', 'Civil/Environmental']

  addCustomer() {
    // console.log('catgroup', this.customerData);
    this.isLoading = true;
    this.apiservice.addCustomers(this.customerData).subscribe(res => {
      console.log('add customer response', this.customerData);
      this.customerData = res;
      this.isLoading = false;

      if (this.customerData.data === 'Email already resgister') {
        alert('Email already registered');
      } else {

        if (this.customerData.isSuccess === true) {
          alert('New Customer Added Successfully');
          this.router.navigate(['/customers']);
        } else {
          alert('somthing went Wrong');
        }
      }
    });
  }
  addNewGroup() {

    this.isSHowGroup = true;
    this.isSHowGroup1 = true;
    this.clickMessage = (<HTMLInputElement>document.getElementById('valuee')).value;
  }
  ShowHideButton1() {
    this.isSHowGroup = false;
    this.isSHowGroup1 = false;

  }

  getCustomerGroup() {

    this.apiservice.getCustomerGroup().subscribe(res => {
      this.customerGroup = res;
      console.log('customerGroup', this.customerGroup);
    });
  }

  // onChange(id){
  //   console.log(id)
  //   this.getCustomerGroup(id)
  // }
  ngOnInit() {
    // console.log(this.customerData);
    this.getCustomerGroup();
    // this.getaddCustomer();

    this.addcustomerForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      mobileNo: ['', Validators.required],
      password: ['', Validators.required],
      customerGroup: ['', Validators.required],
      dob: [''],
      anniversary: ['']

    });
  }
  get f() {
    return this.addcustomerForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    return this.addCustomer();
  }

}
