import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddcustomerRoutingModule } from './addcustomer-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AddcustomerRoutingModule
  ]
})
export class AddcustomerModule { }
