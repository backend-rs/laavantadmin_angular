import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ApiService } from 'src/assets/api.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-plans',
  templateUrl: './addplans.component.html',
  styleUrls: ['./addplans.component.scss']
})
export class AddPlansComponent implements OnInit {
  submitted = false;
  addplanForm: FormGroup;
  isNewCategory1 = true;
  isNewPlan = true;
  newplanValue = '';
  isNewFeature = true;
  newfeatureValue = '';
  isLoading = false;

  planData: any = {

    title: '',
    description: '',
    role: 'customer',
    price: '',
    // features: '',
    // // features: [''],
    discountedPrice: '',
    validity: {
      type: '',
      timePeriod: '1'
    },
    features: [{
      id: '',
    }]

  };
  planResponse: any = {};
  plans = [];


  // dropdownList = [];
  // selectedItems = [];
  // dropdownSettings = {};
  // features = [
  //   { name: 'Basic Ordering' },
  //   { name: 'Basic Reporting' },
  //   { name: 'One Vendor' },
  //   { name: 'Up to 3 Vendors' },
  //   { name: '4-10 Vendors' },
  //   { name: 'Multiple Reports' },
  //   { name: 'Assign Purchase Order' },
  // ];

  // featureList = [];

  featuresList: any;


  constructor(private fb: FormBuilder,
    private apiservice: ApiService,
    private router: Router) { }


  ShowHideButton() {
    this.isNewPlan = false;
  }


  addPlan() {
    // debugger;

    this.isLoading = true;
    console.log('add plann', this.planData);
    this.apiservice.addPlan(this.planData).subscribe(res => {
      console.log('this.planData', res);
      this.planResponse = res;
      this.isLoading = false;

      if (this.planResponse.isSuccess === true) {
        alert('New Plan Added Successfully');
        this.router.navigate(['/plans']);
      } else {
        alert('somthing went Wrong');
      }

    });
  }

  getFeaturesList() {
    // console.log('id', this.featuresList);
    this.apiservice.getFeaturesList().subscribe(res => {
      this.featuresList = res;
      this.isLoading = false;

      console.log('featurelist', this.featuresList);
    });
  }


  ngOnInit() {
    // console.log('ngOnInitngOnInitngOnInitngOnInit', this.planData);
    this.getFeaturesList();
    this.addplanForm = this.fb.group({
      title: [''],
      description: [''],
      role: ['customer'],
      price: [''],
      validity: [''],
      discountedPrice: [''],
      features: ['']
      // password: ['', Validators.required],
      // customerGroup: ['', Validators.required],
    });

  }
  // get f() {
  //   return this.addplanForm.controls;
  // }

  onSubmit() {
    this.submitted = true;
    return this.addPlan();


  }

}
