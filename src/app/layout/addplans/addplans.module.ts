import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { MatSelectModule } from '@angular/material/select';




@NgModule({
  declarations: [

  ],
  imports: [
    CommonModule,
    MatSelectModule,
    NgMultiSelectDropDownModule.forRoot()

  ]
})
export class AddPlansModule { }
