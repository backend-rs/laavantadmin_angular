import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddpotentialcustomerComponent } from './addpotentialcustomer.component';

describe('AddpotentialcustomerComponent', () => {
  let component: AddpotentialcustomerComponent;
  let fixture: ComponentFixture<AddpotentialcustomerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddpotentialcustomerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddpotentialcustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
