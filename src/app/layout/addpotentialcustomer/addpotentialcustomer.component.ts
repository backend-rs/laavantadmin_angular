import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

import { Router } from '@angular/router';
import { ApiService } from 'src/assets/api.service';

@Component({
  selector: 'app-addpotentialcustomer',
  templateUrl: './addpotentialcustomer.component.html',
  styleUrls: ['./addpotentialcustomer.component.scss']
})
export class AddpotentialcustomerComponent implements OnInit {
  submitted = false;
  isLoading = false;
  isSHowGroup = true;
  addpotentialcustomerForm: FormGroup;
  credentials = {
    firstName:'',
    lastName:'',
    email: '',
    password: '',
    mobileNo:'',
  };
  constructor(private fb: FormBuilder,
    private apiservice: ApiService,
    private router: Router, ) { }

  ngOnInit() {
    this.addpotentialcustomerForm = this.fb.group({
     
  
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      mobileNo: ['', Validators.required],
      password: ['', Validators.required],
     
      
    });
  }
    get f() {
      return this.addpotentialcustomerForm.controls;
    }
}
