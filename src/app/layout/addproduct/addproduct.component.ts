import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/assets/api.service';
import { Router } from '@angular/router';
import * as xlsx from 'xlsx';



// import { fileURLToPath } from 'url';

@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.component.html',
  styleUrls: ['./addproduct.component.scss']
})
export class AddproductComponent implements OnInit {
  @ViewChild('productCatalog') productCatalog: ElementRef;
  isExcelData = true;

  isShow = true;

  excelData: any;
  arrayBuffer: any;
  file: File;

  addCategory: any = {
    name: '',
    categoryOf: ''
  };


  //   categories: Category[] = [
  //     {id: 1, name: 'shoes'},
  //     {id: 2, name: 'clothes'},
  //     {id: 3, name: 'bags'},
  //  ];
  submitted = false;
  isLoading = false;
  isNewCategory1 = true;
  isNewCategory2 = true;
  isTable = true;

  isSubCategory1 = true;
  variationType: any = '';
  isColor = true;
  isSize = true;
  isShowSubCategory1 = true;
  isShowSubCategory2 = true;


  productSize = '';
  addproductsForm: FormGroup;
  products: any = {};
  colors = [{ type: 'Red', price: 0 },
  { type: 'Green', price: 0 },
  { type: 'Blue', price: 0 },
  { type: 'Yellow', price: 0 },
  { type: 'Orange', price: 0 },
  { type: 'Pink', price: 0 },
  { type: 'Red-Orange', price: 0 },
  { type: 'Purple', price: 0 },


  ];
  size: any = { type: '', price: 0 };



  productsData: any = {
    name: '',
    uom: '',
    image: '',
    note: '',
    description: '',
    quantity: '',
    roundingValue: '',
    category: '',
    subCategory: '',
    costPerEach: '',
    height: '',
    width: '',
    weight: '',
    length: '',
    manufacturer: '',
    rvDescription: '',
    sku: '',
    variation: {
      items: []
    },

  };

  productCategory: any = {};


  clickMessage = '';
  categoryValue = '';
  productSubCategory: any = {};
  importExcelData: any;
  tableData: any;
  // category: any;
  // id: any;
  data: any;
  productData: any;
  customerData: any;
  responsedata: any;
  categoryData: any;
  categoryResponse: any = {};
  category: any;
  res: any;
  // id: any;

  addSubCategory: any = {
    name: '',
    categoryId: ''
  };
  product: any;
  productResponse: any;


  constructor(private fb: FormBuilder,
    private apiservice: ApiService,
    private router: Router,

  ) {

  }


  addProductForm() {
    this.isShow = !this.isShow;
    //  if(this.isShow){
    //    this.isTable=false;
    //  }else{
    //    this.isTable=true;
    //  }
  }
  ShowHideButton1() {
    this.isNewCategory1 = false;
  }
  ShowHideButton2() {
    this.isNewCategory2 = false;
  }
  addNewCategory() {
    // console.log('addcategory',this.addCategory);
    this.isNewCategory1 = true;
    this.clickMessage = (<HTMLInputElement>document.getElementById('valuee')).value;
    this.apiservice.addNewCategory(this.addSubCategory).subscribe(res => {
      //  this.addSubCategory = res;
      // console.log('add category response from server',res)
      this.isLoading = false;
      console.log('add category response from server', res);
      if (this.addSubCategory.isSuccess === true) {
        alert('New Category Added Successfully');
      } else {
        alert('New SubCategory Added Successfully');
        this.router.navigate(['/addproduct']);
      }

    });
  }


  addNewCategory1() {
    this.isNewCategory2 = true;
    this.isLoading = true;
    this.categoryValue = (<HTMLInputElement>document.getElementById('valuee')).value;
    this.apiservice.addNewCategory1(this.addCategory).subscribe(res => {

      //  this.addCategory = res;
      this.isLoading = false;
      console.log('add category response from server', res);
      if (this.addCategory.isSuccess === true) {
        alert('New Category Added Successfully');
        // this.router.navigate(['/addproduct'])
      } else {
        alert('New Category Added Successfully');
        this.router.navigate(['/addproduct']);
      }

    });

  }
  onChangeVariation() {
    if (this.variationType === 'Size') {
      this.isColor = false;
      this.isSize = true;

    } if (this.variationType === 'Color') {
      this.isColor = true;
      this.isSize = false;
    }
  }


  // onSelectCategory(){
  //   if(this.productsData.category==='Concrete'){
  //    this.isShowSubCategory1=false;
  //    this.isShowSubCategory2=true;

  //   }

  //    if(this.productsData.category==='Demolition'){
  //     this.isShowSubCategory1=true;
  //        this.isShowSubCategory2=false;

  //   }
  // }


  // onChangeSubCategory(){
  //   if (this.productsData.subCategory==='Marking Paint'){
  //    this.isSize = false;}
  //    else{
  //      this.isSize = true;
  //    }

  // }


  onAddColor(data, isChecked: boolean) {
    if (isChecked) {

      this.productsData.variation.items.push(data);

    } else {
      const index = this.productsData.variation.items.indexOf(data);
      this.productsData.variation.items.splice(index, 1);

    }
  }



  getProductCategory() {
    console.log('id', this.productsData);
    this.apiservice.getProductCategory().subscribe(res => {
      this.productCategory = res;
      console.log('productcategory', this.productCategory);
    });
  }


  getProductSubCategory(id) {
    // console.log('id', this.productsData);
    this.apiservice.getProductSubCategory(id).subscribe(res => {
      this.productSubCategory = res;

      console.log('Subcategory', res);
    });
  }
  //  start import excel

  incomingfile(event) {
    this.isShow = true;
    this.isTable = false;
    this.file = event.target.files[0];
    const fileReader = new FileReader();
    fileReader.onload = (e) => {
      this.arrayBuffer = fileReader.result;
      const data = new Uint8Array(this.arrayBuffer);
      const arr = new Array();
      for (let i = 0; i !== data.length; ++i) { arr[i] = String.fromCharCode(data[i]); }
      const bstr = arr.join('');
      const workbook = xlsx.read(bstr, { type: 'binary' });
      const first_sheet_name = workbook.SheetNames[0];
      const worksheet = workbook.Sheets[first_sheet_name];
      this.excelData = xlsx.utils.sheet_to_json(worksheet, { raw: true });
      console.log(this.excelData);

      for (const data of this.excelData) {
        console.log(data);

        const variations: any = [{ type: data.variation1, price: 0 },
        { type: data.variation2, price: 0 },
        { type: data.variation3, price: 0 },
        { type: data.variation4, price: 0 },
        { type: data.variation5, price: 0 },
        { type: data.variation6, price: 0 },
        { type: data.variation7, price: 0 },
        { type: data.variation8, price: 0 },

        ];

        this.importExcelData = {
          name: data.Product,
          uom: data.UOM,
          description: data.Description,
          quantity: data.Quantity,
          costPerEach: data.CostPerEach,
          note: data.Note,
          category: data.Category,
          roundingValue: data.roundingValue,
          height: data.Height,
          width: data.Width,
          weight: data.Weight,
          length: data.Length,
          manufacturer: data.Manufacturer,
          sku: data.SKU,
          subCategory: data.subCategory,
          rvDescription: data.rvDescription,
          image: data.Image,
          variation: {
            items: []
          },
        };

        for (const color of variations) {
          if (color.type) {
            variations.name = 'Color';
            this.importExcelData.variation.items.push(color);
          } else {
            const index = this.importExcelData.variation.items.indexOf(color);
            this.productsData.variation.items.splice(index, 1);
          }
        }


        this.isLoading = true;
        console.log('excel data', this.importExcelData);
        this.apiservice.addProducts(this.importExcelData).subscribe(res => {
          this.products = res;
          this.isLoading = false;
          if (this.products.isSuccess === true) {
            alert('product added successfully');
            this.router.navigate(['/products']);
          } else {
            if (this.products.error === 'product already exist') {
              alert(this.products.error);
            }
            // else{
            //   alert('somthing went wrong');
            // }
          }
        });
      }
    };
    fileReader.readAsArrayBuffer(this.file);
  }



  //      addMultiProducts(){
  //       var retrievedObject:any = localStorage.getItem('testObject');

  //        for(let data of retrievedObject){


  //       let variations:any = [{type: data.variation1, price: 0},
  //         {type:data.variation2, price: 0},
  //         {type: data.variation3, price: 0},
  //         {type: data.variation4, price: 0},
  //         {type:data.variation5, price: 0},
  //         {type: data.variation6, price: 0},
  //         {type: data.variation7, price: 0},
  //         {type: data.variation8, price: 0},

  // ];

  //         this.importExcelData = {
  //         name: data.Product ,
  //         uom: data.UOM,
  //         description:data.Description,
  //         quantity: data.Quantity,
  //         costPerEach: data.CostPerEach,
  //         note: data.Note,
  //         category: data.Category,
  //         roundingValue: data.RoundingValue,
  //         height: data.Height,
  //         width: data.Width,
  //         weight: data.Weight,
  //         length: data.Length,
  //         manufacturer: data.Manufacturer,
  //         sku: data.SKU,
  //         subCategory: data.subCategory,
  //         rvDescription:data.rvDescription,
  //         image: data.Image,
  //         variation: {
  //            items: []
  //         },
  //       };
  //               for (const color of variations){
  //           if(color.type){
  //             variations.name='Color'
  //           this.importExcelData.variation.items.push(color); }
  //         else{
  //           const index =this.importExcelData.variation.items.indexOf(color);
  //           this.productsData.variation.items.splice(index, 1);
  //         }}



  //       this.isLoading=true;
  // tslint:disable-next-line:max-line-length
  //       // if(data.Product.length > 0 && data.Category.length > 0 && data.rvDescription.length > 0 && data.SKU.length > 0 && data.CostPerEach.length > 0 && data.Image.length > 0 && data.subCategory.length > 0 && data.UOM.length > 0){
  //       this.apiservice.addProducts(this.importExcelData).subscribe(res => {
  //         this.products=res;
  //         this.isLoading=false;
  //         console.log(this.products);
  //       });
  //       if (this.products.isSuccess === true) {
  //         alert('product updated successfully');
  //         this.router.navigate(['/products']);}

  //         else {
  //           if(this.products.error='product already exist'){
  //               alert('some products already exist');

  //              this.router.navigate(['/products']);
  //             break; }
  //              else{
  //               alert('something went wrong');
  //               break;

  //              }
  //      }

  // }

  //  }

  //  setObjectTo(data){
  //   var testObject = data;
  //   console.log('local stored excel data ',testObject);
  //   localStorage.setItem('testObject', JSON.stringify(testObject));
  //   console.log('localStorage',localStorage);
  //   return this.addMultiProducts();

  //  }
  //  getTableData(){
  //   var table:any = document.getElementById('displayTable');
  //   var rowCount = table.rows.length;
  //   for (var i = 1; i < rowCount - 1; i++) {
  //        var row = table.rows[i]["Limit"].ToString();
  //        console.log('table data ',row);

  //   }


  // }

  onChange(id) {
    console.log(id);
    this.getProductSubCategory(id);
    this.addSubCategory.categoryId = id;

  }


  ngOnInit() {
    this.getProductCategory();
    //  this.getProductSubCategory();

    this.addproductsForm = this.fb.group({
      name: ['', Validators.required],
      category: ['', Validators.required],

      description: ['', Validators.required],
      rvDescription: ['', Validators.required],

      sku: ['',],
      manufacturer: ['',],
      costPerEach: ['',],
      quantity: ['',],
      // quantityInn: ['', ],
      variation: ['',],
      height: ['',],
      length: ['',],
      // roundingvalue: ['', ],
      width: ['',],
      weight: ['',],
      image: ['',],
      productSize: ['',],
      subCategory: ['',],
      uom: ['',],
      roundingValue: ['',],
      categoryId: ['']
      // image: ['', Validators.required],
      // status: ['', Validators.required],



    });
  }


  get f() {
    return this.addproductsForm.controls;
  }


  addProducts() {
    // console.log('catgroup',this.productsData);

    if (this.isSize) {
      this.size.type = this.productSize;
      this.productsData.variation.items.push(this.size);
    }
    if (this.productsData.variation.items.length > 0) {
      console.log('productdata', this.productsData);
      this.isLoading = true;
      this.apiservice.addProducts(this.productsData).subscribe(res => {
        this.productResponse = res;
        // console.log('productresponse', this.products);
        this.isLoading = false;

        if (this.productResponse.isSuccess === true) {
          alert('product added successfully');
          this.router.navigate(['/products']);
        } else {

          alert('somthing went wrong');
        }
      });
    } else {
      alert('variations required');
    }

  }



  onSubmit() {
    this.submitted = true;
    return this.addProducts();
  }

}
