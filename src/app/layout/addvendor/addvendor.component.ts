import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/assets/api.service';
import { Router } from '@angular/router';
import { fileURLToPath } from 'url';

@Component({
  selector: 'app-addvendor',
  templateUrl: './addvendor.component.html',
  styleUrls: ['./addvendor.component.scss']
})
export class AddvendorComponent implements OnInit {

  submitted = false;
  isLoading = false;
  isSHowGroup = true;
  addvendorForm: FormGroup;
  vendorData: any = {
    firstName: '',
    lastName: '',
    email: '',
    role: 'vendor',
    mobileNo: '',
    password: '',
    // customerGroup: '',
  };
  productCategory: any = {};
  customerResponse: any = {};
  clickMessage: string;
  isSHowGroup1 = true;
  customerGroup: any;
  id: any;
  responseData: any;
  getaddVendor: any;
  getCustomerGroup: any;
  vendorResponse: any;
  constructor(private fb: FormBuilder,
    private apiservice: ApiService,
    private router: Router, ) { }


  addVendor() {
    console.log('catgroup', this.vendorData);
    this.isLoading = true;
    this.apiservice.addVendors(this.vendorData).subscribe(res => {
      // console.log('add customer response',res);
      this.vendorResponse = res;
      this.isLoading = false;

      if (this.vendorResponse.message === 'Email already resgister') {
        alert('Email already registered');
      } else {

        if (this.vendorResponse.isSuccess === true && this.vendorResponse.message !== 'Email already resgister') {
          alert('New Vendor Added Successfully');
          this.router.navigate(['/vendors']);
        } else {
          alert('somthing went Wrong');
        }
      }
    });
  }



  ngOnInit() {
    console.log(this.vendorData);
    //  this.getCustomerGroup();
    // this.getaddCustomer();

    this.addvendorForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      mobileNo: ['', Validators.required],
      password: ['', Validators.required],
      // customerGroup: ['',Validators.required],

    });
  }
  get f() {
    return this.addvendorForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    return this.addVendor();
  }


}
