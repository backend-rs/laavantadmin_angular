import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddvendorRoutingModule } from './addvendor-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AddvendorRoutingModule
  ]
})
export class AddvendorModule { }
