import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignvendorComponent } from './assignvendor.component';

describe('VendorsComponent', () => {
  let component: AssignvendorComponent;
  let fixture: ComponentFixture<AssignvendorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignvendorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignvendorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
