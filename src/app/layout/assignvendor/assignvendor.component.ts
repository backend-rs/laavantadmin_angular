import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/assets/api.service';
import { DataService } from 'src/app/data.service';
import { Router } from '@angular/router';
import { PageEvent } from '@angular/material';
import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-vendors',
  templateUrl: './assignvendor.component.html',
  styleUrls: ['./assignvendor.component.scss']
})

export class AssignvendorComponent implements OnInit {
  vendorList = [];
  pageNo = 1;
  isLoading = false;
  productId = {};


  pageEvent: PageEvent;
  pageSize: number;
  length = 100;
  pageSizeOptions = [10, 20, 30, 40, 50];
  vendor: any;



  constructor(private apiservice: ApiService,  private dataservice: DataService, private router: Router, private http: HttpClient, ) {
    this.isLoading = true;
    this.productId = dataservice.getOption();
    this.isLoading = false;

   }

  getVendors() {
    this.isLoading = true;
    this.apiservice.getVendors(this.pageNo, this.pageSize).subscribe(res => {
      this.vendorList = res;
      this.isLoading = false;

      });
    }

    assignVendor(data) {

      const vendor = {
        logo: '',
        vendorId: data

      };
          this.apiservice.assignVendor(this.productId, vendor).subscribe(res => {
             console.log(res);

            alert('product assigned');
            this.router.navigate(['/products']);

            }); }


    getNext(event: PageEvent) {
      console.log('pageSize', event);

      this.pageSize = event.pageSize;
          this.pageNo = event.pageIndex + 1;
         this.apiservice.getVendors( this.pageNo, this.pageSize).subscribe(res => {
                this.vendorList = res;
                        this.isLoading = false;


            });
          }

    ngOnInit() {
    this.getVendors();
  }

}
