import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AssignvendorRoutingModule } from './assignvendor-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AssignvendorRoutingModule
  ]
})
export class VendorsModule { }
