import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatDialog, MatDialogConfig } from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { identifierModuleUrl } from '@angular/compiler';
import * as myJson from 'src/assets/data/users.json';
import { Router } from '@angular/router';
import { DataService } from 'src/app/data.service';
import { ApiService } from 'src/assets/api.service';
import { ChartType } from 'chart.js';
//  import { Label} from 'ng2-charts';

import * as pluginDataLabels from 'chartjs-plugin-datalabels';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  pageNo = 1;
  usersLength;
  vendorsData = [];
  customersData = [];
  data: any;
  barChartLabels: any = [];
  barChartData: any = [];
  pageSize: any;
  productsData = [];
  isLoading: Boolean = false;
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  // public barChartData = [
  //   {data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A'},
  //   {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'}
  // ];
  //  public barChartLabels:string[] = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;

  // public barChartPlugins = [pluginDataLabels];

  public barChartColors: Array<any> = [
    {
      backgroundColor: '#3f51b5',

    },
    {
      backgroundColor: '#4CAF50',
    },

  ];
  // events
  public chartClicked(e: any): void {
    // console.log(e);
  }

  public chartHovered(e: any): void {
    // console.log(e);
  }
  constructor(public matDialog: MatDialog,
    private http: HttpClient,
    private router: Router,
    private apiservice: ApiService) {

  }

  // public barChartData = [];
  // openModal1(data) {

  //     const dialogConfig = new MatDialogConfig();

  //     dialogConfig.disableClose = false;
  //     dialogConfig.id = 'modal-component';
  //     dialogConfig.height = '650px';
  //     dialogConfig.width = '600px';
  //     dialogConfig.data = data;
  //     // https://matersial.angular.io/components/dialog/overview
  //     const modalDialog = this.matDialog.open(Modal1Component, dialogConfig);

  //   }

  getGraphData() {
    this.isLoading = true;

    this.apiservice.graphData().subscribe(res => {
      this.data = res;
      this.isLoading = false;
      this.data.data.barChartData.forEach(element => {
        // let item ={
        //   data:[],
        //   label:''
        // }
        this.barChartData.push(element
        );
        this.barChartLabels = this.data.data.labels;
      });
      console.log('graphdata', this.barChartData);
      //       let barChartData = [
      //         {data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A'},
      //         {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'}
      //       ];
      //       let barChartLabels= ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
      // this.barChartData=barChartData;
      // this.barChartLabels= barChartLabels;

    });

  }

  getUsers() {

    this.apiservice.getCustomers().subscribe(res => {
      this.customersData = res;
    });
  }

  getVendors() {

    this.apiservice.getTotalVendors().subscribe(res => {
      this.vendorsData = res;
    });
  }

  getCustomers() {

    this.apiservice.getTotalCustomers().subscribe(res => {
      this.customersData = res;
    });
  }

  getProducts() {

    this.apiservice.getTotalProducts().subscribe(res => {
      this.productsData = res;
    });
  }

  ngOnInit() {
    // this.getUsers();
    this.getVendors();
    this.getCustomers();
    this.getProducts();
    this.getGraphData();

  }

}
