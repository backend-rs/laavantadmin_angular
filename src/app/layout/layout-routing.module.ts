import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { UserupdateComponent } from './userupdate/userupdate.component';
import { UsersComponent } from './users/users.component';
import { ProductsComponent } from './products/products.component';
import { AddproductComponent } from './addproduct/addproduct.component';
import { AssignvendorComponent } from './assignvendor/assignvendor.component';
import { VendorsComponent } from './vendors/vendors.component';
import { VendorproductsComponent } from './vendorproducts/vendorproducts.component';
import { AddcustomerComponent } from './addcustomer/addcustomer.component';
import { ProductupdateComponent } from './productupdate/productupdate.component';
import { AddPlansComponent } from './addplans/addplans.component';
import { PaymentsComponent } from './payments/payments.component';
import { AddvendorComponent } from './addvendor/addvendor.component';
import { PlansComponent } from './plans/plans.component';
import { PlanupdateComponent } from './planupdate/planupdate.component';
import { PotentialcustomerComponent } from './potentialcustomer/potentialcustomer.component';
import { AddpotentialcustomerComponent } from './addpotentialcustomer/addpotentialcustomer.component';
import { UpdatevendorComponent } from './updatevendor/updatevendor.component';



const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [

            {
                path: '',
                redirectTo: 'dashboard'
            },

            {
                path: 'dashboard',
                loadChildren: './dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'userupdate',
                loadChildren: './userupdate/userupdate.module#UserupdateModule'
            },

            {
                path: 'customers',
                component: UsersComponent
            },
            {
                path: 'addplans',
                component: AddPlansComponent
            },
            {
                path: 'payments',
                component: PaymentsComponent
            },
            {
                path: 'products',
                component: ProductsComponent
            },
            {
                path: 'addproduct',
                component: AddproductComponent
            },
            {
                path: 'assignvendor',
                component: AssignvendorComponent
            },
            {
                path: 'vendors',
                component: VendorsComponent
            },
            {
                path: 'vendorproducts',
                component: VendorproductsComponent
            },
            {
                path: 'addcustomer',
                component: AddcustomerComponent
            },
            {
                path: 'productupdate',
                component: ProductupdateComponent
            },

            {
                path: 'addvendor',
                component: AddvendorComponent
            },

            {
                path: 'plans',
                component: PlansComponent
            },

            {
                path: 'planupdate',
                component: PlanupdateComponent
            },
     
            {
                path: 'potentialcustomer',
                component: PotentialcustomerComponent
            },
         
            {
                path: 'addpotentialcustomer',
                component: AddpotentialcustomerComponent
            },
 
            // {
            //     path: 'updatevendor',
            //     component: UpdatevendorComponent
            // },


        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class LayoutRoutingModule { }
