import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatDatepickerModule,
    MatDatepicker,
    MatPaginatorModule,
    MatCardModule,
    MatSelectModule,
    MatInput,
    MatFormFieldControl,
    MatFormFieldModule,
    MatCheckboxModule,

} from '@angular/material';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { TranslateModule } from '@ngx-translate/core';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { TopnavComponent } from './components/topnav/topnav.component';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { NavComponent } from './nav/nav.component';
import { UsersComponent } from './users/users.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { ProductsComponent } from './products/products.component';
import { AddproductComponent } from './addproduct/addproduct.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AssignvendorComponent } from './assignvendor/assignvendor.component';
import { VendorsComponent } from './vendors/vendors.component';
import { VendorproductsComponent } from './vendorproducts/vendorproducts.component';
import { AddcustomerComponent } from './addcustomer/addcustomer.component';
import { HttpClientModule } from '@angular/common/http';
import { ProductupdateComponent } from './productupdate/productupdate.component';
import { AddPlansComponent } from './addplans/addplans.component';
import { PaymentsComponent } from './payments/payments.component';
import { AddvendorComponent } from './addvendor/addvendor.component';
import { ChartsModule } from 'ng2-charts';
import { PlansComponent } from './plans/plans.component';
import { PlanupdateComponent } from './planupdate/planupdate.component';
import { UserupdateComponent } from './userupdate/userupdate.component';
import { PotentialcustomerComponent } from './potentialcustomer/potentialcustomer.component';
import { MatSlideToggleModule, MatSlideToggle } from '@angular/material/slide-toggle';
import { AddpotentialcustomerComponent } from './addpotentialcustomer/addpotentialcustomer.component';
import { UpdatevendorComponent } from './updatevendor/updatevendor.component';




@NgModule({
    imports: [
        CommonModule,
        LayoutRoutingModule,
        MatToolbarModule,
        MatButtonModule,
        MatSidenavModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatListModule,
        MatSelectModule,
        NgxPaginationModule,
        ReactiveFormsModule,
        TranslateModule,
        MatPaginatorModule,
        MatCardModule,
        HttpClientModule,
        ChartsModule,
        MatSelectModule,
        FormsModule,
        MatFormFieldModule,
        MatSlideToggleModule,
        MatCheckboxModule,
        NgMultiSelectDropDownModule.forRoot(),

    ],
    declarations: [UsersComponent,
        LayoutComponent,
        NavComponent,
        TopnavComponent,
        SidebarComponent,
        ProductsComponent,
        AddproductComponent,
        AssignvendorComponent,
        VendorsComponent,
        VendorproductsComponent,
        AddcustomerComponent,
        AddvendorComponent,
        ProductupdateComponent,
        AddPlansComponent,
        PaymentsComponent,
        PlansComponent,
        PlanupdateComponent,
        UserupdateComponent,
        PotentialcustomerComponent,
        AddpotentialcustomerComponent,
        UpdatevendorComponent
    ]

})
export class LayoutModule { }
