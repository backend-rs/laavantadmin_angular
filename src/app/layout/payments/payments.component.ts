import { Component, OnInit, EventEmitter, Output, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss']
})
export class PaymentsComponent implements OnInit {
  @Output() pageChange: EventEmitter<number>;
  @Output() pageBoundsCorrection: EventEmitter<number>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  isLoading = false;
  // pageNo = 1;
  // pageEvent: PageEvent;
  // pageSize: number;
  // length = 100;
  // pageSizeOptions = [5,10, 20, 30, 40, 50];

  usersData = [
    { name: 'user', plan: 'standard', date: '02-01-20', amount: '05' },
    { name: 'michel', plan: 'standard', date: '02-02-20', amount: '05' },
    { name: 'shivam', plan: 'standard', date: '02-01-20', amount: '05' },
    { name: 'nikita', plan: 'professional', date: '04-06-20', amount: '14' },
    { name: 'john', plan: 'standard', date: '22-01-20', amount: '05' },
    { name: 'deep', plan: 'professional', date: '21-10-20', amount: '14' },
    { name: 'kuldeep', plan: 'professional', date: '08-01-20', amount: '14' },
    { name: 'rushali', plan: 'standard', date: '02-01-20', amount: '05' },
    { name: 'simran', plan: 'standard', date: '02-01-20', amount: '05' },
    { name: 'isha', plan: 'professional', date: '02-06-20', amount: '14' },
    { name: 'jacson', plan: 'professional', date: '02-07-19', amount: '14' },
    { name: 'satnam', plan: 'standard', date: '12-01-20', amount: '05' },
    { name: 'amrit', plan: 'professional', date: '11-07-20', amount: '14' },

  ];


  constructor() { }


  //   getNext(event: PageEvent) {
  //     this.length=event.length;
  //    this.pageNo = event.pageIndex + 1;
  //    this.pageSize = event.pageSize;
  //   //  console.log('len',this.productsList.length);




  // };

  ngOnInit() {
  }

}
