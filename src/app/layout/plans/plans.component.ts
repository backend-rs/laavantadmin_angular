import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DataService } from 'src/app/data.service';
import { Router } from '@angular/router';
import { MatDialog, MatTableDataSource, MatPaginator, PageEvent } from '@angular/material';
import { ApiService } from 'src/assets/api.service';


@Component({
  selector: 'app-plans',
  templateUrl: './plans.component.html',
  styleUrls: ['./plans.component.scss']
})
export class PlansComponent implements OnInit {
  @Output() pageChange: EventEmitter<number>;
  @Output() pageBoundsCorrection: EventEmitter<number>;
  @ViewChild(MatPaginator) paginator: MatPaginator;



  isLoading: Boolean = false;

  planList = [];
  // planData = [];
  pageNo = 1;
  //  itemOption = [5, 10, 25, 50];


  // pageLength: number;
  // pageSize: 5;
  // pageSizeOptions = [5, 10, 25];
  pageEvent: PageEvent;
  datasource: null;
  pageSize: number;
  length = 100;
  pageSizeOptions = [10, 20, 30, 40, 50];
  planResponse: any = {};
  data: any = [];
  usersData = [];

  sendDate: any;

  loadPage: any;
  isSelected = false;
  featuresList:any;
  planById:any;
  userData: any;

  // page: any;


  constructor(public matDialog: MatDialog,
    private http: HttpClient,
    private router: Router,
    private dataservice: DataService,
    private apiservice: ApiService) {

  }



  // openModal1(data) {

  //     const dialogConfig = new MatDialogConfig();

  //     dialogConfig.disableClose = false;
  //     dialogConfig.id = 'modal-component';
  //     dialogConfig.height = '650px';
  //     dialogConfig.width = '600px';
  //     dialogConfig.data = data;
  //     // https://material.angular.io/components/dialog/overview
  //     const modalDialog = this.matDialog.open(Modal1Component, dialogConfig);

  //   }

  onPass(data) {
    this.sendDate = data;
    // this.dataservice.setOption(data);
    this.router.navigate(['/planupdate']);
  }

  getPlanById(id) {
    this.isLoading = true;
    this.apiservice.getPlanById(id).subscribe(res => {
      // console.log('list', res);
      this.isSelected = false;
      // console.log('get list response', responseData);
      this.planById = res;
      this.isLoading = false;

    });

  }

  getdata($event) {
    this.loadPage = $event;
    if (this.loadPage) {
      this.isSelected = true;
      this.getPlanList();
    }
  }


  getPlanList() {
    this.isLoading = true;
    this.apiservice.getPlanList().subscribe(res => {
      console.log('planlist', res);
      // console.log('get list response', responseData);
      this.planList = res;

      this.isLoading = false;

    });

  }
  getFeaturesList() {
    // console.log('id', this.featuresList);
    this.apiservice.getFeaturesList().subscribe(res => {
      this.featuresList = res;
      this.isLoading = false;

      console.log('featurelist', this.featuresList);
    });
  }


  getFeaturesById(id) {
    this.isLoading = true;
    console.log('id', this.featuresList);
    this.apiservice.getFeaturesById(id).subscribe(res => {
      // console.log('list', res);
      // this.isSelected = false;
      // console.log('get list response', responseData);
      // this.featureById = res;
      this.featuresList = res;
      this.isLoading = false;
      console.log('featurebyid======', this.featuresList);

    });

  }
  getNext(event: PageEvent) {
    console.log('pageSize', event);
    this.pageSize = event.pageSize;
    this.pageNo = event.pageIndex + 1;

    this.apiservice.getUsers(this.pageNo, this.pageSize).subscribe(res => {
      this.userData = res;
      this.isLoading = false;


    });
  }


  ngOnInit() {

    this.getPlanList();



  }

}
