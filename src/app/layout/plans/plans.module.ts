import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlansRoutingModule } from './plans-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    PlansRoutingModule
  ]
})
export class PlansModule { }
