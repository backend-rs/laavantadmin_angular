import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { updateBinding } from '@angular/core/src/render3/instructions';
import { Route } from '@angular/compiler/src/core';
import { Validators, FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Headers } from '@angular/http';
import { Location } from '@angular/common';
import { DataService } from 'src/app/data.service';
import { ApiService } from 'src/assets/api.service';
import { MatFormFieldControl } from '@angular/material';

@Component({
  selector: 'app-planupdate',
  templateUrl: './planupdate.component.html',
  styleUrls: ['./planupdate.component.scss']
})
export class PlanupdateComponent implements OnInit {

  @Input()
  plan: any = {};

  @Output()
  isChanged: EventEmitter<any> = new EventEmitter();

  isLoading: Boolean = false;
  headers: Headers;
  getData: any = [];
  data: any = {};
  planupdateForm: FormGroup;
  submitted = false;
  featuresList: any;

  feature: any = {};
  planData: any;
  planResponse: any;
  selectedFeature: any;
  selected: any = {};
  features: any = [];
  selectFeature: any = [];
  toppings = new FormControl();
  featuresData: any;
  changeFeature: any = [];

  constructor(private http: HttpClient,
    private fb: FormBuilder,
    private router: Router,
    private dataservice: DataService,
    private apiservice: ApiService) {
    this.isLoading = true;
    // this.plan = dataservice.getOption();
    this.isLoading = false;
  }




  getfeaturesList() {
    // console.log('id', this.featuresList.id);
    this.isLoading = true;
    this.apiservice.getFeaturesList().subscribe(res => {
      this.featuresData = res;
      res.forEach(item => {
        this.features.push(item.name);
      });
      console.log('features::::', this.features);
      this.isLoading = false;

    });
  }


  ngOnInit() {

    this.selected = this.plan.features;
    this.selected.forEach(element => {
      this.selectFeature.push(element.id.name);
    });

    console.log('selectFeature', this.selectFeature);
    this.getfeaturesList();
    // this.addPlanFeature();
    // this.updateFeaturesList();
    this.planupdateForm = this.fb.group({
      title: [''],
      description: [''],
      role: ['customer'],
      price: [''],
      validity: [''],
      discountedPrice: [''],
      features: ['']
      // password: ['', Validators.required],
      // customerGroup: ['', Validators.required],
    });
  }


  updatePlan() {
    // console.log('this.plan', this.plan);
    // this.selectedFeature.forEach(element => {
    //   this.featuresData.forEach(item => {
    //     if (item.name === element) {
    //       this.changeFeature.push(item);
    //     }
    //   });
    // });
    console.log('changeFeature', this.changeFeature);

    this.plan.features = null;
    this.plan.features = this.changeFeature;

    // console.log('features@@@@@', this.plan.features);
    this.isLoading = true;
    this.apiservice.updatePlan(this.plan.id, this.plan).subscribe(res => {
      this.isLoading = false;
      console.log('updateeeeeeeeplann', res);
      // console.log('features@@@@@============', this.plan.features.id);
      this.plan = res;
      this.selected = this.plan.data.features;

      if (this.plan.isSuccess === true) {
        alert('Plan updated successfully');
        this.router.navigate(['./plans']);
        this.isChanged.emit(this.plan);

      } else {
        alert('something went wrong');
      }
    });
  }
  onSubmit() {
    console.log('onSubmitonSubmitonSubmitonSubmit', this.selected);
    this.submitted = true;
    this.plan.features = this.selected;
    return this.updatePlan();
  }

  onChangeFeature(feature) {
    console.log('onchangefeature', feature);
    this.selectedFeature = feature;

  }
}
