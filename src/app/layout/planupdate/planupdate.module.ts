import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlanupdateRoutingModule } from './planupdate-routing.module';
import { MatInputModule, MatInput, MatFormFieldModule, MatFormField, MatSelectModule } from '@angular/material';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    PlanupdateRoutingModule,
    MatFormFieldModule,
    MatFormField,
    MatInputModule,
    MatSelectModule,
    FormsModule
  ]
})
export class PlanupdateModule { }
