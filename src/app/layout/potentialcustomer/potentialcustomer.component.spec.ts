import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PotentialcustomerComponent } from './potentialcustomer.component';

describe('PotentialcustomerComponent', () => {
  let component: PotentialcustomerComponent;
  let fixture: ComponentFixture<PotentialcustomerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PotentialcustomerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PotentialcustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
