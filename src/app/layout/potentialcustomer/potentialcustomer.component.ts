import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { DataService } from 'src/app/data.service';
import { ApiService } from 'src/assets/api.service';


@Component({
  selector: 'app-potentialcustomer',
  templateUrl: './potentialcustomer.component.html',
  styleUrls: ['./potentialcustomer.component.scss']
})
export class PotentialcustomerComponent implements OnInit {
  isLoading: Boolean = false;
  usersData = [];
  checked = false;
  disabled = false; 

  constructor(
    private http: HttpClient,
    private router: Router,
    private dataservice: DataService,
    private apiservice: ApiService
  ) { }
  edit(){

  }

  deleteUser(){

  }

  userProfile(){
    
  }

  ngOnInit() {
  }

}
