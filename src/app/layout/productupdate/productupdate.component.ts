import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from 'src/app/data.service';
import { ApiService } from 'src/assets/api.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { updateBinding } from '@angular/core/src/render3/instructions';
@Component({
  selector: 'app-productupdate',
  templateUrl: './productupdate.component.html',
  styleUrls: ['./productupdate.component.scss']
})
export class ProductupdateComponent implements OnInit {
  excelData: any;
  arrayBuffer: any;
  file: File;
  product: any = {};
  submitted = false;
  isLoading = false;
  isNewCategory1 = true;
  isNewCategory2 = true;
  isSubCategory1 = true;
  variationType: any = '';
  isColor = true;
  isSize = true;
  isShowSubCategory1 = true;
  isShowSubCategory2 = true;
  data: any = {};

  // excelData: any;
  // arrayBuffer: any;
  // file: File;

  addCategory: any = {
    name: '',
    categoryOf: ''
  };


  productSize = '';
  updateProductForm: FormGroup;

  colors = [{ type: 'Red', price: 0 },
  { type: 'Green', price: 0 },
  { type: 'Blue', price: 0 },
  { type: 'Yellow', price: 0 },
  { type: 'Orange', price: 0 },
  { type: 'Pink', price: 0 },
  { type: 'Red-Orange', price: 0 },
  { type: 'Purple', price: 0 },


  ];

  size: any = { type: '', price: 0 };

  productCategory: any = {};

  clickMessage = '';
  categoryValue = '';
  productSubCategory: any = {};
  importExcelData: any;

  productRes: any = {};

  id: any;

  productdata: any = {};


  addSubCategory: any = {
    name: '',
    categoryId: ''
  };

  productResponse: any;
  addProduct: any;
  products: any;
  productData: any;
  category: any;
  subCategoryId: any;
  selectedSubCategory: any;
  selected: '';



  constructor(private fb: FormBuilder,

    private router: Router,
    private http: HttpClient,
    private dataservice: DataService,
    private apiservice: ApiService) {
    this.isLoading = true;
    this.product = dataservice.getOption();
    this.isLoading = false;
  }


  // addProductForm() {
  //   this.isShow = !this.isShow;
  //  if(this.isShow){
  //    this.isTable=false;
  //  }else{
  //    this.isTable=true;
  //  }
  // }
  ShowHideButton1() {
    this.isNewCategory1 = false;
  }
  ShowHideButton2() {
    this.isNewCategory2 = false;
  }

  addNewCategory() {
    // console.log('addcategory',this.addCategory);
    this.isNewCategory1 = true;
    this.clickMessage = (<HTMLInputElement>document.getElementById('valuee')).value;
    this.apiservice.addNewCategory(this.addSubCategory).subscribe(res => {
      //  this.addSubCategory = res;
      // console.log('add category response from server',res)
      this.isLoading = false;
      console.log('add category response from server', res);
      if (this.addSubCategory.isSuccess === true) {
        alert('New SubCategory Added Successfully');
      } else {
        alert('Something went wrong');
        this.router.navigate(['/addproduct']);
      }

    });
  }


  addNewCategory1() {
    this.isNewCategory2 = true;
    this.isLoading = true;
    this.categoryValue = (<HTMLInputElement>document.getElementById('valuee')).value;
    this.apiservice.addNewCategory1(this.addCategory).subscribe(res => {

      //  this.addCategory = res;
      this.isLoading = false;
      console.log('add category response from server', res);
      if (this.addCategory.isSuccess === true) {
        alert('New Category Added Successfully');
        // this.router.navigate(['/addproduct'])
      } else {
        alert('Something went wrong');
        this.router.navigate(['/addproduct']);
      }

    });

  }
  onChangeVariation() {
    if (this.variationType === 'Size') {
      this.isColor = false;
      this.isSize = true;

    } if (this.variationType === 'Color') {
      this.isColor = true;
      this.isSize = false;
    }
  }


  // onSelectCategory(){
  //   if(this.productsData.category==='Concrete'){
  //    this.isShowSubCategory1=false;
  //    this.isShowSubCategory2=true;

  //   }

  //    if(this.productsData.category==='Demolition'){
  //     this.isShowSubCategory1=true;
  //        this.isShowSubCategory2=false;

  //   }
  // }


  // onChangeSubCategory(){
  //   if (this.productsData.subCategory==='Marking Paint'){
  //    this.isSize = false;}
  //    else{
  //      this.isSize = true;
  //    }

  // }


  onAddColor(data, isChecked: boolean) {
    if (isChecked) {

      this.productdata.variation.items.push(data);

    } else {
      const index = this.productdata.variation.items.indexOf(data);
      this.productdata.variation.items.splice(index, 1);

    }
  }



  getProductCategory() {
    // console.log('id', this.productdata);
    this.apiservice.getProductCategory().subscribe(res => {
      this.productCategory = res;
    });
  }



  getProductSubCategory(id) {
    this.apiservice.getProductSubCategory(id).subscribe(res => {
      this.productSubCategory = res;
      // this.subCategoryId = id;

      // console.log('subcategory.......', this.product.category.id);
    });
  }
  //  start import excel





  //      addMultiProducts(){
  //       var retrievedObject:any = localStorage.getItem('testObject');

  //        for(let data of retrievedObject){


  //       let variations:any = [{type: data.variation1, price: 0},
  //         {type:data.variation2, price: 0},
  //         {type: data.variation3, price: 0},
  //         {type: data.variation4, price: 0},
  //         {type:data.variation5, price: 0},
  //         {type: data.variation6, price: 0},
  //         {type: data.variation7, price: 0},
  //         {type: data.variation8, price: 0},

  // ];

  //         this.importExcelData = {
  //         name: data.Product ,
  //         uom: data.UOM,
  //         description:data.Description,
  //         quantity: data.Quantity,
  //         costPerEach: data.CostPerEach,
  //         note: data.Note,
  //         category: data.Category,
  //         roundingValue: data.RoundingValue,
  //         height: data.Height,
  //         width: data.Width,
  //         weight: data.Weight,
  //         length: data.Length,
  //         manufacturer: data.Manufacturer,
  //         sku: data.SKU,
  //         subCategory: data.subCategory,
  //         rvDescription:data.rvDescription,
  //         image: data.Image,
  //         variation: {
  //            items: []
  //         },
  //       };
  //               for (const color of variations){
  //           if(color.type){
  //             variations.name='Color'
  //           this.importExcelData.variation.items.push(color); }
  //         else{
  //           const index =this.importExcelData.variation.items.indexOf(color);
  //           this.productsData.variation.items.splice(index, 1);
  //         }}



  //       this.isLoading=true;
  // tslint:disable-next-line:max-line-length
  //       // if(data.Product.length > 0 && data.Category.length > 0 && data.rvDescription.length > 0 && data.SKU.length > 0 && data.CostPerEach.length > 0 && data.Image.length > 0 && data.subCategory.length > 0 && data.UOM.length > 0){
  //       this.apiservice.addProducts(this.importExcelData).subscribe(res => {
  //         this.products=res;
  //         this.isLoading=false;
  //         console.log(this.products);
  //       });
  //       if (this.products.isSuccess === true) {
  //         alert('product updated successfully');
  //         this.router.navigate(['/products']);}

  //         else {
  //           if(this.products.error='product already exist'){
  //               alert('some products already exist');

  //              this.router.navigate(['/products']);
  //             break; }
  //              else{
  //               alert('something went wrong');
  //               break;

  //              }
  //      }

  // }

  //  }

  //  setObjectTo(data){
  //   var testObject = data;
  //   console.log('local stored excel data ',testObject);
  //   localStorage.setItem('testObject', JSON.stringify(testObject));
  //   console.log('localStorage',localStorage);
  //   return this.addMultiProducts();

  //  }
  //  getTableData(){
  //   var table:any = document.getElementById('displayTable');
  //   var rowCount = table.rows.length;
  //   for (var i = 1; i < rowCount - 1; i++) {
  //        var row = table.rows[i]["Limit"].ToString();
  //        console.log('table data ',row);

  //   }


  // }

  // onChange(id) {
  //   this.getProductSubCategory(this.id);
  //   this.addSubCategory.categoryId = id;

  // }



  ngOnInit() {
    this.getProductCategory();
    this.getProductSubCategory(this.product.category.id);
    // this.updateProduct();

    this.updateProductForm = this.fb.group({
      // id: [''],
      name: [''],
      description: [''],
      quantity: [''],
      costPerEach: [''],
      category: [''],
      image: [''],
      height: [''],
      width: [''],
      weight: [''],
      length: [''],
      subCategory: [''],
      rvDescription: [''],
      manufacturer: [''],
      uom: [''],
      status: [''],
      variation: [''],
      // uom: [''],
      // roundingValue: [''],
      categoryId: [''],
      subCategoryId: ['']

    });
  }



  // get f() {
  //   return this.updateProductForm.controls;
  // }


  updateProduct() {

    this.isLoading = true;
    this.product.category = this.product.category.id;
    this.product.subCategory = this.selectedSubCategory;
    console.log('product=====....', this.product);
    this.apiservice.updateProduct(this.product.id, this.product).subscribe(res => {
      this.isLoading = false;
      this.product = res;

      console.log('update===>product', this.product);
      if (this.product.isSuccess === true) {
        alert('product updated successfully');
        this.router.navigate(['/products']);
      } else {
        alert('something went wrong please try again!');
      }

    });

  }



  // updateProductImage(){
  // this.isLoading=true;
  //   this.apiservice.updateProductImage(this.product.id,this.product).subscribe(res => {
  //     this.isLoading=false;
  //     this.productRes=res;
  //     // console.log(this.productRes);
  //     if(this.productRes.isSuccess===true){
  //       alert('product updated successfully');
  //       this.router.navigate(['/products']);
  //     }else{
  //       alert('something went wrong please try again!');
  //     }

  //    });

  // }


  onSubmit() {
    this.submitted = true;
    // this.product.subCategory = this.selected;

    // this.productSubCategory.data.forEach(element => {
    //   if (element.id === this.selectedSubCategory.id) {
    //     this.product.subCategory = element.id;
    //   }
    // });
    return this.updateProduct();
  }

  onChange(item) {
    console.log('onchange', item);
    this.selectedSubCategory = item;
  }
}

