import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductupdateRoutingModule } from './productupdate-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ProductupdateRoutingModule
  ]
})
export class ProductupdateModule { }
