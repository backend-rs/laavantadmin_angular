import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from 'src/app/data.service';
import { ApiService } from 'src/assets/api.service';


@Component({
  selector: 'app-updatevendor',
  templateUrl: './updatevendor.component.html',
  styleUrls: ['./updatevendor.component.scss']
})
export class UpdatevendorComponent implements OnInit {
  isLoading: Boolean = false;
  vendorupdateForm: FormGroup;
  vendor: any = {};
  submitted: boolean = false;


  constructor(private http: HttpClient,
    private formBuilder: FormBuilder,
    private router: Router,
    private dataservice: DataService,
    private apiservice: ApiService) {

}
updateVendor(){
  
}
  ngOnInit() {
    this.vendorupdateForm = this.formBuilder.group({
      firstName: [''],
      lastName: [''],
      mobileNo: [''],
      country: [''],
      address: [''],
      dob: [''],
      anniversary: [''],
      // role: [''],
      customerGroup: ['']

});
    
  }
  onSubmit() {
    this.submitted = true;
    return this.updateVendor();
  }
}
