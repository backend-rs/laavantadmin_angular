import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DataService } from 'src/app/data.service';
import { Router } from '@angular/router';
import { MatDialog, MatTableDataSource, MatPaginator, PageEvent, ThemePalette } from '@angular/material';
import { ApiService } from 'src/assets/api.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  @Output() pageChange: EventEmitter<number>;
  @Output() pageBoundsCorrection: EventEmitter<number>;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  user: any = {};

  isLoading: Boolean = false;
  color: ThemePalette = 'primary';
  checked = false;
  disabled = false;
  usersData = [];
  pageNo = 1;
  //  itemOption = [5, 10, 25, 50];


  // pageLength: number;
  // pageSize: 5;
  // pageSizeOptions = [5, 10, 25];
  pageEvent: PageEvent;
  datasource: null;
  pageSize: number;
  length = 100;
  pageSizeOptions = [10, 20, 30, 40, 50];
  sendDate: any;
  loadPage: any;
  isSelected: Boolean = false;
  userById:any;
  customerGroup: any = [];

  // tslint:disable-next-line:quotemark




  // page: any;


  constructor(public matDialog: MatDialog,
    private http: HttpClient,
    private router: Router,
    private dataservice: DataService,
    private apiservice: ApiService) {

  }



  // openModal1(data) {

  //     const dialogConfig = new MatDialogConfig();

  //     dialogConfig.disableClose = false;
  //     dialogConfig.id = 'modal-component';
  //     dialogConfig.height = '650px';
  //     dialogConfig.width = '600px';
  //     dialogConfig.data = data;
  //     // https://material.angular.io/components/dialog/overview
  //     const modalDialog = this.matDialog.open(Modal1Component, dialogConfig);

  //   }

  onPass(data) {
    this.sendDate = data;
    console.log(' this.sendDate', data);
    // this.dataservice.setOption(data);
    this.router.navigate(['/userupdate']);

  }

  getCustomerGroup() {

    this.apiservice.getCustomerGroup().subscribe(res => {
      this.customerGroup = res;
      console.log('customerGroup', this.customerGroup);
    });
  }


  getUserById(id) {
    this.isLoading = true;
    this.apiservice.getUserById(id).subscribe(res => {
      this.isSelected = false;
      // console.log('user::getUserById', res);
      this.userById = res;
      this.isLoading = false;

    });

  }

  getdata($event) {
    // console.log('getdata........');
    this.loadPage = $event;
    if (this.loadPage) {
      this.isSelected = true;
      this.getUsers();
    }
  }



  getUsers() {
    this.isLoading = true;

    this.apiservice.getUsers(this.pageNo, this.pageSize).subscribe(res => {
      this.usersData = res;
      this.isLoading = false;
      // console.log('getUsers........', res);

    });

  }

  getNext(event: PageEvent) {
    console.log('pageSize', event);
    this.pageSize = event.pageSize;
    this.pageNo = event.pageIndex + 1;

    this.apiservice.getUsers(this.pageNo, this.pageSize).subscribe(res => {
      this.usersData = res;
      this.isLoading = false;


    });
  }

  

  ngOnInit() {

    this.getUsers();



  }

}

