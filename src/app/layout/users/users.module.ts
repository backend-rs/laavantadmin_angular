import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxPaginationModule } from 'ngx-pagination';
import { UsersRoutingModule } from './users-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { UsersComponent } from './users.component';
import { MatGridListModule, MatCardModule, MatButtonModule, MatTableModule, MatIconModule, MatPaginatorModule, MatSlideToggleModule, MatMenuModule, MatCheckboxModule } from '@angular/material';
import { StatModule } from 'src/app/shared/modules/stat/stat.module';
import { UserupdateComponent } from '../userupdate/userupdate.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [UsersComponent, UserupdateComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    MatGridListModule,
    StatModule,
    MatCardModule,
    MatCardModule,
    MatTableModule,
    MatButtonModule,
    MatIconModule,
    NgxPaginationModule,
    MatPaginatorModule,
    MatCardModule,
    MatSlideToggleModule,
    MatMenuModule,
    FormsModule,
    MatCheckboxModule,
    FlexLayoutModule.withConfig({ addFlexToParent: false })
  ]
})
export class UsersModule { }
