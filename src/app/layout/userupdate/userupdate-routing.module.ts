import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserupdateComponent } from './userupdate.component';

const routes: Routes = [
    {
        path: '',
        component: UserupdateComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UserupdateRoutingModule {}
