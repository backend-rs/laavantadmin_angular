
import { Component, Inject, Optional, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { updateBinding } from '@angular/core/src/render3/instructions';
import { Route } from '@angular/compiler/src/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Headers } from '@angular/http';
import { Location } from '@angular/common';
import { DataService } from 'src/app/data.service';
import { ApiService } from 'src/assets/api.service';
import { User } from '../../models/user.model';



@Component({
  selector: 'app-userupdate',
  templateUrl: './userupdate.component.html',
  styleUrls: ['./userupdate.component.scss']
})
export class UserupdateComponent implements OnInit {

  @Input()
  user: any = {};

  @Output()
  isChanged: EventEmitter<any> = new EventEmitter();

  isLoading: Boolean = false;

  // user: any = {};
  // selectedValue: [''];
  headers: Headers;
  getData: any = [];
  data: any = {};
  userupdateForm: FormGroup;
  customerGroup: any;
  submitted = false;
  selectCustomer: any;
  customersGroup: any = [];
  selectedGroup: any;
  customerGroupName: any;
  selected = '';
  // usersData: any = {
  //   firstName: '' ,
  //   lastName: '',
  //   mobileNo: '',
  //   country: '',
  //   address: '',
  //   dob: '',
  //   anniversary: '',
  //   role: ''
  // };
  constructor(private http: HttpClient,
    private formBuilder: FormBuilder,
    private router: Router,
    private dataservice: DataService,
    private apiservice: ApiService) {

    // debugger;
    this.isLoading = true;
    // this.user = dataservice.getOption();
    this.isLoading = false;


  }


  ngOnInit() {
    // console.log('ngOnInitngOnInitngOnInitngOnInit', this.user);
    this.selected = this.user.customerGroup.id;
    // tslint:disable-next-line:no-debugger
    // debugger;
    console.log(this.user);

    this.getCustomerGroup();


    this.userupdateForm = this.formBuilder.group({
      firstName: [''],
      lastName: [''],
      mobileNo: [''],
      country: [''],
      address: [''],
      dob: [''],
      anniversary: [''],
      // role: [''],
      customerGroup: ['']

   });
  }
  get f() {
    return this.userupdateForm.controls;
  }


  updateUser() {
    // console.log('user::update::this.user.id', this.user.id);
    // console.log('user::update::this.user', this.user);
    // console.log('group@@@@@', this.user.customerGroup);
    this.isLoading = true;
    this.user.anniversary = this.user.otherInformation.anniversary;
    this.apiservice.updateUser(this.user.id, this.user).subscribe(res => {
      this.isLoading = false;
      console.log('update user', res);
      this.user = res;
      this.selected = this.user.data.customerGroup.id;
      if (this.user.isSuccess === true) {
        alert('data updated successfully');
        this.router.navigate(['/customers']);
        this.isChanged.emit(this.user);
      } else {
        alert('something went wrong');
      }
    });
  }

  getCustomerGroup() {
    this.apiservice.getCustomerGroup().subscribe(res => {
      this.customersGroup = res;
      // console.log('customersGroup', this.customersGroup);
    });
  }



  onSubmit() {
    console.log('onSubmitonSubmitonSubmitonSubmit', this.selected);
    // this.customerGroup.name = this.customerGroup;
    this.submitted = true;
    this.user.customerGroup = this.selected;
    return this.updateUser();
  }

  onChangeGroup(group) {
    console.log('onchange', group);
    this.selectedGroup = group;
  }
}
