import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserupdateComponent } from './userupdate.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { UserupdateRoutingModule } from './userupdate-routing.module';
import {
  MatGridListModule,
  MatCardModule,
  MatTableModule,
  MatButtonModule,
  MatIconModule,
  MatFormFieldModule,
  MatOptionModule,
  MatSelectModule,
  MatInputModule,
  MatFormField
} from '@angular/material';
import { StatModule } from 'src/app/shared/modules/stat/stat.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [UserupdateComponent],
  imports: [
    CommonModule,
    UserupdateRoutingModule,
    ReactiveFormsModule,
    MatGridListModule,
    StatModule,
    MatCardModule,
    MatTableModule,
    MatButtonModule,
    MatIconModule,
    MatFormFieldModule,
    MatSelectModule,
    MatOptionModule,

    MatInputModule,
    FlexLayoutModule.withConfig({ addFlexToParent: false })
  ]
})
export class UserupdateModule { }
