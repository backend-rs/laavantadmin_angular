import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/assets/api.service';
import { DataService } from 'src/app/data.service';
import { PageEvent } from '@angular/material';

@Component({
  selector: 'app-vendorproducts',
  templateUrl: './vendorproducts.component.html',
  styleUrls: ['./vendorproducts.component.scss']
})
export class VendorproductsComponent implements OnInit {
isLoading: Boolean = false;
  vendor: any;
  productsList = [];

  pageEvent: PageEvent;
  pageSize: number;
  length = 100;
  pageSizeOptions = [10, 20, 30, 40, 50];
  pageNo: number;

  constructor(private apiservice: ApiService, dataservice: DataService) {
    this.isLoading = true;
    this.vendor = dataservice.getOption();
    this.isLoading = false;

   }

  getVendorProducts() {
    this.isLoading = true;

    this.apiservice.getVendorProducts(this.vendor.id, this.pageNo, this.pageSize).subscribe(res => {
           this.productsList = res;
           this.isLoading = false;
    });
  }

  getNext(event: PageEvent) {
    console.log('pageSize', event);
    this.pageSize = event.pageSize;
     this.pageNo = event.pageIndex + 1;

    this.apiservice.getVendorProducts(this.vendor.id, this.pageNo, this.pageSize).subscribe(res => {
      this.productsList = res;
              this.isLoading = false;


  });
}
  ngOnInit() {
    this.getVendorProducts();
  }

}
