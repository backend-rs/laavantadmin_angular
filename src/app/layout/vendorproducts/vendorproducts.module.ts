import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VendorproductsRoutingModule } from './vendorproducts-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    VendorproductsRoutingModule
  ]
})
export class VendorproductsModule { }
