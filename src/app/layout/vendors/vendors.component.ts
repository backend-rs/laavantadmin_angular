import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/assets/api.service';
import { DataService } from 'src/app/data.service';
import { Router } from '@angular/router';
import { PageEvent, ThemePalette } from '@angular/material';

@Component({
  selector: 'app-vendors',
  templateUrl: './vendors.component.html',
  styleUrls: ['./vendors.component.scss']
})
export class VendorsComponent implements OnInit {
isLoading: Boolean = false;
pageNo = 1;
vendorList = [];

pageEvent: PageEvent;
pageSize: number;
length = 100;
pageSizeOptions = [10, 20, 30, 40, 50];
color: ThemePalette = 'primary';
checked = false;
disabled = false;

  // usersData:any

  constructor(private apiservice: ApiService,
    private dataservice: DataService,
    private router: Router) { }

  getVendors() {
    this.isLoading = true;
    this.apiservice.getVendors(this.pageNo, this.pageSize).subscribe(res => {
      this.vendorList = res;
      this.isLoading = false;

      });
    }

    getNext(event: PageEvent) {
      console.log('pageSize', event);
      this.pageSize = event.pageSize;
       this.pageNo = event.pageIndex + 1;

      this.apiservice.getVendors(this.pageNo, this.pageSize).subscribe(res => {
        this.vendorList = res;
                this.isLoading = false;


    });
  }
    onPass(data) {

      this.dataservice.setOption(data);
      this.router.navigate(['/vendorproducts']);

  }


  ngOnInit() {
    this.getVendors();
  }

}
