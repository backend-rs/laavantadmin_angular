import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CanActivate } from '@angular/router';
import { stringToKeyValue } from '@angular/flex-layout/extended/typings/style/style-transforms';
import { AuthGuard } from '../shared/guard';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { tokenKey } from '@angular/core/src/view';
import { ApiService } from 'src/assets/api.service';



@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    userData: any = [];
    isLoading: Boolean = false;
    credentials = {
        email: '',
        password: ''

    };
    loginForm: FormGroup;
    submitted: Boolean = false;

    constructor(private router: Router,
        private fb: FormBuilder,
        private http: HttpClient,
        private apiservice: ApiService) { }

    login() {


        if (this.submitted) {
            this.isLoading = true;

            this.apiservice.login(this.credentials).subscribe(res => {
                if (res.isSuccess === false) {
                    alert('Wrong username or password!');
                    return this.isLoading = false;
                } else {
                    this.userData = res;

                    this.isLoading = false;

                    localStorage.setItem('token', this.userData.token);
                    if (this.userData.role === 'admin') {
                        this.router.navigate(['/dashboard']);
                    } else { alert('you are not admin'); }
                }
            });



        }

    }


    ngOnInit() {


        this.loginForm = this.fb.group({
            email1: ['', Validators.required],
            password1: ['', Validators.required]

        });
    }

    get f() {
        return this.loginForm.controls;
    }


    onSubmit() {
        this.submitted = true;
        return this.login();
    }

}

