


export class Category {

  id: string;
  name: string;
  categoryOf: string;
  isShow: boolean;
  customerGroup: string;
  categoryId: string;
  subCategory: string;
  subCategoryId: string;
  constructor(obj?: any) {

    if (!obj) {
      return;
    }
    this.id = obj.id;
    this.name = obj.name;
    this.categoryOf = obj.categoryOf;
    this.categoryId = obj.categoryId;
    this.isShow = obj.isShow;
    this.customerGroup = obj.customerGroup;
    this.subCategory = obj.subCategory;
    this.subCategoryId = obj.subCategoryId;



  }


}
