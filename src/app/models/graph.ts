


export class Graph {

    id: string;
    deviceToken: string;
    isSuccess: boolean;
    labels: string;
    barChartData: string;
  
    


  constructor(obj?: any) {

    if (!obj) {
      return;
    }
    this.id = obj.id;
    this.deviceToken = obj.deviceToken;
    this.isSuccess = obj.isSuccess;
    this.labels= obj.labels;
    this.barChartData = obj.barChartData;
    



  }
}
