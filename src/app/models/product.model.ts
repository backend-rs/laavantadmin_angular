import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';



export class Product {

  id: string;
  name: string;
  uom: string;
  description: string;
  quantity: string;
  price: string;
  note: string;
  category: string;
  image: string;
  pdf: string;
  status: string;
  costPerEach: string;
  overAllPrice: string;
  height: string;
  width: string;
  weight: string;
  length: string;
  manufacturer: string;
  sku: string;
  subCategory: string;
  roundingValue: string;
  rvDescription: string;
  // categoryId: string;
  // subCategoryId: string;
  variation: {
    name: string,
    items: []
  };
  categoryOf: string;
  isShow: boolean;
  customerGroup: string;

  // assignVendor: [{
  //   logo: string;
  //   id: string;
  // }];




  constructor(obj?: any) {

    if (!obj) {
      return;
    }
    this.id = obj.id;
    this.name = obj.name;
    this.description = obj.description;
    this.quantity = obj.quantity;
    this.price = obj.price;
    this.note = obj.note;
    this.category = obj.category;
    this.image = obj.image;
    this.pdf = obj.pdf;
    this.status = obj.status;
    this.costPerEach = obj.costPerEach;
    this.overAllPrice = obj.overAllPrice;
    this.height = obj.height;
    this.width = obj.width;
    this.weight = obj.weight;
    this.length = obj.length;
    this.manufacturer = obj.manufacturer;
    this.sku = obj.sku;
    this.subCategory = obj.subCategory;
    this.uom = obj.uom;
    this.roundingValue = obj.roundingValue;
    // this.categoryId = obj.categoryId;
    // this.subCategoryId = obj.subCategoryId;
    this.rvDescription = obj.rvDescription;
    this.categoryOf = obj.categoryOf;
    this.customerGroup = obj.customerGroup;






    // this.assignVendor = obj.assignVendor;
    this.variation = obj.variation;


  }
}
