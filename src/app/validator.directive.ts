import { Directive, Input } from '@angular/core';
import { Validator, NG_VALIDATORS, AbstractControl, Validators } from '@angular/forms';
import { from } from 'rxjs';

@Directive({
  selector: '[appValidator]',
  providers: [{
  provide: NG_VALIDATORS,
  useExisting: ValidatorDirective,
  multi: true
  }]
})
export class ValidatorDirective implements Validator {
  @Input() appValidator: string;

  validate(control: AbstractControl): {[key: string]: any} |null {
    const controlToCompare = control.parent.get(this.appValidator);
    if (controlToCompare && controlToCompare.value !== control.value) {
    return { 'notEqual': true};
    }
    return null;
    }
    }


