import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs/internal/Observable';
import { User } from 'src/app/models/user.model';
import { Product } from 'src/app/models/product.model';
import { Subject } from 'rxjs/internal/Subject';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpEventType, HttpEvent } from '@angular/common/http';
import { Vendor } from 'src/app/models/vendor.model';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Category } from 'src/app/models/category.model';
import { map } from 'rxjs/operators';
import { Graph } from 'src/app/models/graph';
import { Plan } from 'src/app/models/plan.model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  root = environment.apiUrls.reports;
  userResponse: any = {};
  productResponse: any = {};
  categoryResponse: any = {};
  customerResponse: any = {};
  subCategoryResponse: any = {};
  productCategory: any = {};
  productdata: any;
  responseData: any;
  vendorResponse: any = {};
  token: string | string[];
  productRes: any = {};
  graphResponse: any = {};
  planResponse: any = {};
  featureResponse: any = {};
  planByIdResponse: any = {};
  userByIdResponse: any = {};
  featuresByIdResponse: any = {};




  constructor(private http: HttpClient) { }




  login(model): Observable<User> {
    const subject = new Subject<User>();
    this.http.post(`${this.root}/users/login`, model, { headers: null }).subscribe((responseData: any) => {
      // if (responseData.statusCode !== 200) {
      //   throw new Error('This request has failed ' + responseData.statusCode);
      // }
      // const dataModel = responseData.json();

      // if (!dataModel.isSuccess) {
      //   if (responseData.statusCode === 200) {
      //     throw new Error(dataModel.code || dataModel.message || 'failed');
      //   } else {
      //     throw new Error(responseData.statusCode + '');
      //   }
      // }

      subject.next(responseData.data);
    }, (error) => {
      // console.log('errrrrrrrrrr', error.error);
      subject.next(error.error);

    });

    return subject.asObservable();

  }


  getUsers(pageNo, pageSize): Observable<User[]> {
    const subject = new Subject<User[]>();

    // tslint:disable-next-line:max-line-length
    this.http.get(`${this.root}/users/list?pageNo= ${pageNo}&pageSize= ${pageSize}&role=customer`, { headers: null }).subscribe((responseData) => {
      this.userResponse = responseData;


      subject.next(this.userResponse.items);
    });

    return subject.asObservable();

  }



  getCustomers(): Observable<User[]> {
    const subject = new Subject<User[]>();

    // tslint:disable-next-line:max-line-length
    this.http.get(`${this.root}/users/list?pageNo=1&pageSize=1000&role=customer`, { headers: null }).subscribe((responseData) => {
      this.userResponse = responseData;


      subject.next(this.userResponse.data);
    });

    return subject.asObservable();

  }


  getTotalCustomers(): Observable<User[]> {
    const subject = new Subject<User[]>();

    // tslint:disable-next-line:max-line-length
    this.http.get(`${this.root}/users/list?pageNo=1&pageSize=1000&role=customer`, { headers: null }).subscribe((responseData) => {
      this.userResponse = responseData;


      subject.next(this.userResponse.items);
    });

    return subject.asObservable();

  }


  updateUser(id, user): Observable<User[]> {
    // tslint:disable-next-line:no-debugger
    // debugger;
    const subject = new Subject<User[]>();
    const token = localStorage.getItem('token');

    const header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': token
      })

    };

    this.http.put(`${this.root}/users/update/${id}`, user, header).subscribe((responseData) => {
      this.userResponse = responseData;
      console.log('updateeeeeee', responseData);
      // console.log('api  to response', this.userResponse);


      subject.next(this.userResponse);
    });

    return subject.asObservable();

  }


  getVendors(pageNo, pageSize): Observable<Vendor[]> {
    const subject = new Subject<Vendor[]>();

    // tslint:disable-next-line:max-line-length
    this.http.get(`${this.root}/users/list?pageNo= ${pageNo}&pageSize= ${pageSize}&role=vendor`, { headers: null }).subscribe((responseData) => {
      this.userResponse = responseData;


      subject.next(this.userResponse.items);
    });

    return subject.asObservable();

  }

  getTotalVendors(): Observable<Vendor[]> {
    const subject = new Subject<Vendor[]>();

    // tslint:disable-next-line:max-line-length
    this.http.get(`${this.root}/users/list?pageNo=1&pageSize=1000&role=vendor`, { headers: null }).subscribe((responseData) => {
      this.userResponse = responseData;


      subject.next(this.userResponse.items);
    });

    return subject.asObservable();

  }



  getProducts(pageNo, pageSize): Observable<Product[]> {
    const subject = new Subject<Product[]>();


    // inprogress

    this.http.get(`${this.root}/products/list?pageNo=${pageNo}&pageSize=${pageSize}&role=admin`).subscribe((responseData) => {
      this.productResponse = responseData;
      subject.next(this.productResponse.items);
    }, (error) => {
      // console.log('errrrrrrrrrr', error.error);
      subject.next(error.error);


    });

    return subject.asObservable();

  }



  getTotalProducts(): Observable<Product[]> {
    const subject = new Subject<Product[]>();

    this.http.get(`${this.root}/products/list?pageNo=1&pageSize=500`).subscribe((responseData) => {
      this.productResponse = responseData;
      subject.next(this.productResponse.items);

    });

    return subject.asObservable();

  }

  // updateProductImage(id,data): Observable<Product[]> {
  //   const subject = new Subject<Product[]>();
  //   const token = localStorage.getItem('token');

  //   const header = {
  //     headers: new HttpHeaders({
  //         'Content-Type': 'application/json',
  //         'x-access-token': token
  //     })

  //   };

  //    this.http.put(`${this.root}/uploads/image/product/${id}`, header ).subscribe((responseData) => {
  //     this.productResponse = responseData;
  //     console.log('image', responseData)
  //     subject.next(this.productResponse.items);
  //   }, (error) => {
  //     subject.next(error.error);
  //        console.log('errrrrrrrrrr', error.error);

  // }  );
  //   return subject.asObservable();
  // }

  // uploadProductImage(id, model): Observable<any> {
  //   const subject = new Subject<any>();
  //   const token = localStorage.getItem('token');
  //   const header = {
  //     headers: new HttpHeaders({
  //       'enctype': 'multipart/form-data',
  //       'Accept': 'application/json',
  //       'x-access-token': this.token
  //     })
  //   };
  //   this.http.put(`${this.root}/uploads/image/product/${id}`, model, header).subscribe((responseData) => {
  //     this.productResponse = responseData;
  //     console.log('imagesssss', this.productResponse);
  //     subject.next(this.productResponse.items);

  //   }, (error) => {
  //     subject.next(error.error);
  //     console.log('errrrrrrrrrr', error.error);

  //   });
  //   return subject.asObservable();
  // }


  //============================== uploadpdf========================================>

  uploadPDF(id, model): Observable<any> {
    const subject = new Subject<any>();
    const token = localStorage.getItem('token');
    const header = {
      headers: new HttpHeaders({
        'enctype': 'multipart/form-data',
        'Accept': 'application/json',
        'x-access-token': this.token
      })
    };
    this.http.put(`${this.root}/products/uploadPdf/${id}`, model, header).subscribe((responseData) => {
      this.productResponse = responseData;
      console.log('pdffff', this.productResponse);
      subject.next(this.productResponse.items);

    }, (error) => {
      subject.next(error.error);
      console.log('errrrrrrrrrr', error.error);

    });
    return subject.asObservable();
  }


  // updateUserImage(data): Observable<Product[]> {
  //   const subject = new Subject<Product[]>();

  //    this.http.put(`${this.root}/users/uploadProfilePic` , data ).subscribe((responseData) => {
  //     this.productResponse = responseData;
  //     subject.next(this.productResponse.items);
  //   }, (error) => {
  //     subject.next(error.error);

  // }  );
  //   return subject.asObservable();
  // }





  getProductCategory(): Observable<Product[]> {
    const subject = new Subject<Product[]>();

    const token = localStorage.getItem('token');

    const header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': token
      })

    };

    this.http.get(`${this.root}/categories`, header).subscribe((responseData) => {
      this.categoryResponse = responseData;
      console.log('category', responseData);
      subject.next(this.categoryResponse);
    });

    return subject.asObservable();

  }


  getProductSubCategory(id): Observable<Product[]> {
    const subject = new Subject<Product[]>();
    const token = localStorage.getItem('token');
    const header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': token
      })

    };
    console.log('id', id);
    this.http.get(`${this.root}/subCategories/getByCategory?id=${id}`, header).subscribe((responseData) => {
      this.subCategoryResponse = responseData;
      console.log('subcategory', responseData);

      subject.next(this.subCategoryResponse);
    });

    return subject.asObservable();

  }
  getCustomerGroup(): Observable<Category[]> {
    const subject = new Subject<Category[]>();


    const token = localStorage.getItem('token');

    const header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': token
      })

    };

    this.http.get(`${this.root}/categories`, header).subscribe((responseData) => {
      this.categoryResponse = responseData;
      // console.log('customer', responseData);
      subject.next(this.categoryResponse.data);
    });

    return subject.asObservable();

  }



  getVendorProducts(vendorId, pageNo, pageSize): Observable<Vendor[]> {
    const subject = new Subject<Vendor[]>();


    // inprogress


    // tslint:disable-next-line:max-line-length
    this.http.get(`${this.root}/products/listByVendor?pageNo= ${pageNo}&pageSize= ${pageSize}&vendorId=${vendorId}`).subscribe((responseData) => {
      this.productResponse = responseData;
      subject.next(this.productResponse.items);
    });
    return subject.asObservable();
  }



  addProducts(data): Observable<Product[]> {
    const subject = new Subject<Product[]>();
    const token = localStorage.getItem('token');

    const header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': token
      })

    };
    this.http.post(`${this.root}/products/addProduct`, data, header).subscribe((res) => {
      this.productResponse = res;
      console.log('addproductresponse>>>>>>>>', res);
      subject.next(this.productResponse);

    }, (error) => {
      //  console.log('errrrrrrrrrr', error.error);
      subject.next(error.error);

    });

    return subject.asObservable();

  }


  addNewCategory1(model): Observable<Category[]> {
    const subject = new Subject<Category[]>();
    const token = localStorage.getItem('token');

    const header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': token
      })

    };
    this.http.post(`${this.root}/categories`, model, header).subscribe((res) => {
      this.categoryResponse = res;
      console.log('addcategory', res);
      subject.next(this.categoryResponse);

    }, (error) => {
      console.log('errrrrrrrrrr', error.error);
      subject.next(error.error);

    });

    return subject.asObservable();

  }


  addNewCategory(model): Observable<Category[]> {
    const subject = new Subject<Category[]>();
    const token = localStorage.getItem('token');

    const header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': token
      })

    };
    this.http.post(`${this.root}/subCategories`, model, header).subscribe((res) => {
      this.subCategoryResponse = res;
      console.log('addsubcategory', res);
      subject.next(this.subCategoryResponse);

    }, (error) => {
      console.log('errrrrrrrrrr', error.error);
      subject.next(error.error);

    });

    return subject.asObservable();

  }
  updateProduct(id, model): Observable<Product[]> {
    const subject = new Subject<Product[]>();

    const token = localStorage.getItem('token');

    const header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': token
      })

    };
    // console.log(this.productResponse);
    this.http.put(`${this.root}/products/update/${id}`, model).subscribe((responseData) => {
      this.productResponse = responseData;
      console.log('********', this.productResponse);
      subject.next(this.productResponse);
    }, (error) => {
      console.log('errrrrrrrrrr', error.error);
      subject.next(error.error);

    });



    return subject.asObservable();

  }


  addCustomers(data): Observable<User[]> {
    const subject = new Subject<User[]>();

    const token = localStorage.getItem('token');

    const header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': token
      })

    };
    this.http.post(`${this.root}/users/register`, data).subscribe((responseData) => {
      this.customerResponse = responseData;
      // console.log(this.productResponse);
      subject.next(this.customerResponse);
    }, (error) => {
      console.log('errrrrrrrrrr', error.error);
      subject.next(error.error);

    });

    return subject.asObservable();

  }

  addVendors(data): Observable<User[]> {
    const subject = new Subject<User[]>();

    const token = localStorage.getItem('token');

    const header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': token
      })

    };
    this.http.post(`${this.root}/users/register`, data).subscribe((responseData) => {
      this.vendorResponse = responseData;
      console.log(this.productResponse);
      subject.next(this.vendorResponse);
    }, (error) => {
      console.log('errrrrrrrrrr', error.error);
      subject.next(error.error);

    });

    return subject.asObservable();

  }

  assignVendor(productId, model): Observable<Vendor[]> {
    const subject = new Subject<Vendor[]>();
    this.http.put(`${this.root}/products/asignVendor/${productId}`, model).subscribe((responseData) => {
      this.productResponse = responseData;
      // console.log(this.productResponse);

      subject.next(this.productResponse);

    }, (error) => {
      console.log('errrrrrrrrrr', error.error);
      subject.next(error.error);

    });

    return subject.asObservable();

  }

  graphData(): Observable<Graph[]> {
    const subject = new Subject<Graph[]>();

    const token = localStorage.getItem('token');

    const header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': token
      })

    };
    this.http.get(`${this.root}/counts/monthly/users`, header).subscribe((responseData) => {
      this.graphResponse = responseData;
      console.log(this.graphResponse);
      subject.next(this.graphResponse);
    }, (error) => {
      console.log('errrrrrrrrrr', error.error);
      subject.next(error.error);

    });

    return subject.asObservable();

  }


  addPlan(data): Observable<Plan[]> {
    const subject = new Subject<Plan[]>();

    const token = localStorage.getItem('token');

    const header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': token
      })

    };
    this.http.post(`${this.root}/plans`, data, header).subscribe((responseData) => {
      this.planResponse = responseData;
      // console.log(this.planResponse);
      console.log('plans@@@', responseData);
      subject.next(this.planResponse);
    }, (error) => {
      console.log('errrrrrrrrrr', error.error);
      subject.next(error.error);

    });

    return subject.asObservable();

  }

  getPlanList(): Observable<Plan[]> {
    const subject = new Subject<Plan[]>();
    const token = localStorage.getItem('token');

    const header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': token
      })

    };

    this.http.get(`${this.root}/plans?role=customer&status=active`, header).subscribe((responseData) => {
      this.planResponse = responseData;
      // console.log('planslist', responseData);
      console.log('planlist', this.planResponse);
      subject.next(this.planResponse.data);
    });

    return subject.asObservable();

  }

  getPlanById(id): Observable<Plan[]> {
    const subject = new Subject<Plan[]>();
    const token = localStorage.getItem('token');

    const header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': token
      })

    };

    this.http.get(`${this.root}/plans/getById/${id}`, header).subscribe((responseData) => {
      this.planByIdResponse = responseData;
      // console.log('planslist', responseDa ta);
      // console.log('planlist', this.planByIdResponse);
      subject.next(this.planByIdResponse.data);
    });

    return subject.asObservable();

  }

  getFeaturesList(): Observable<Plan[]> {
    const subject = new Subject<Plan[]>();
    const token = localStorage.getItem('token');

    const header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': token
      })

    };

    this.http.get(`${this.root}/features?role=customer&status=active`, header).subscribe((responseData) => {
      this.featureResponse = responseData;
      // console.log('featurelist', responseData);
      console.log('featurelist', this.featureResponse);
      subject.next(this.featureResponse.data);
    });

    return subject.asObservable();

  }

  updatePlan(id, data: any): Observable<Plan[]> {
    const subject = new Subject<Plan[]>();
    const token = localStorage.getItem('token');

    const header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': token
      })

    };

    this.http.put(`${this.root}/plans/${id}`, data, header).subscribe((responseData) => {
      this.planResponse = responseData;
      console.log('updated plan', responseData);
      // console.log('api  to response', this.planResponse);


      subject.next(this.planResponse);
    });

    return subject.asObservable();

  }
  updateFeaturesList(id, plan): Observable<Plan[]> {
    const subject = new Subject<Plan[]>();
    const token = localStorage.getItem('token');

    const header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': token
      })

    };

    this.http.put(`${this.root}/features/${id}`, plan, header).subscribe((responseData) => {
      this.featureResponse = responseData;
      console.log('updated feature', responseData);
      // console.log('api  to response', this.planResponse);


      subject.next(this.featureResponse);
    });

    return subject.asObservable();

  }

  getUserById(id): Observable<User[]> {
    const subject = new Subject<User[]>();
    const token = localStorage.getItem('token');

    const header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': token
      })

    };

    this.http.get(`${this.root}/users/getById/${id}`, header).subscribe((responseData) => {
      this.userByIdResponse = responseData;
      // console.log('userlist=====', responseData);
      // console.log('userlist', this.userByIdResponse);
      subject.next(this.userByIdResponse.data);
    });

    return subject.asObservable();

  }

  getFeaturesById(id): Observable<Plan[]> {
    const subject = new Subject<Plan[]>();
    const token = localStorage.getItem('token');

    const header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': token
      })

    };

    this.http.get(`${this.root}/features/getById/${id}`, header).subscribe((responseData) => {
      this.featuresByIdResponse = responseData;
      // console.log('planslist', responseDa ta);
      console.log('featuresbyId', this.featuresByIdResponse);
      subject.next(this.featuresByIdResponse.data);
    });

    return subject.asObservable();

  }

  // addPlanFeature(data): Observable<Plan[]> {
  //   const subject = new Subject<Plan[]>();

  //   const token = localStorage.getItem('token');

  //   const header = {
  //     headers: new HttpHeaders({
  //       'Content-Type': 'application/json',
  //       'x-access-token': token
  //     })

  //   };
  //   this.http.post(`${this.root}/plans/features/add`, data, header).subscribe((responseData) => {
  //     this.planResponse = responseData;
  //     console.log(this.planResponse);
  //     // console.log('plans', responseData);
  //     subject.next(this.planResponse);
  //   }, (error) => {
  //     console.log('errrrrrrrrrr', error.error);
  //     subject.next(error.error);

  //   });

  //   return subject.asObservable();

  // }

}

